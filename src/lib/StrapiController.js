class StrapiController {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
        this.axios = require('axios')
        this.qs = require('qs')
        // this.strapiToken = require('./StrapiToken.json')
    }

    GetDeck(deckCode, getCardData) {
        let jsonBody = {
            filters: {
                DeckCode: {
                    $eq: deckCode,
                },
            }
        }
        if (getCardData) {
            jsonBody.populate = {
                cards: {
                    populate: ['Card'],
                }
            }
        }
        let query = this.qs.stringify(jsonBody, {
            encodeValuesOnly: true,
        });
        let url = `${this.baseUrl}/api/decks?${query}`;
        console.log(url);
        return this.axios.get(url)
            .then(res => {
                if (res?.data?.meta?.pagination?.total === 1) {
                    return this.cleanUpStrapiResponse(res.data.data[0], this)
                }
            })
            .catch(error => console.error(error))
    }

    cleanUpStrapiResponse(data, self) {
        if (data === null || data === undefined || typeof data === 'string') {
            return data;
        }

        if (Array.isArray(data)) {
            for (let i = 0; i < data.length; i++) {
                data[i] = self.cleanUpStrapiResponse(data[i], self)
            }
        } else {
            data = this.cleanUpStrapiNode(data, self)
            Object.keys(data).forEach(function (key) {
                data[key] = self.cleanUpStrapiResponse(data[key], self)
            });
        }
        return data
    }

    cleanUpStrapiNode(data, self) {
        let cleanUped = false;
        let cleanUpList = ['attributes', 'data']
        cleanUpList.forEach(key => {
            if (data[key] != undefined) {
                Object.keys(data[key]).forEach(function (innerKey) {
                    data[innerKey] = data[key][innerKey]
                });
                data[key] = undefined
                cleanUped = true;
            }
        });

        if (cleanUped) {
            data = self.cleanUpStrapiNode(data, self)
        }

        return data;
    }

    getExpandedDeck(deck) {
        let deckClone = JSON.parse(JSON.stringify(deck))
        let rawData = []
        deckClone.cards.forEach(card => {
            for (let i = 0; i < card.Count; i++) {
                rawData.push(card.Card)
            }
        });
        deckClone.rawData = rawData
        return deckClone
    }

}

module.exports = StrapiController;