import io from 'socket.io-client';
import Card from '../helpers/card';
import Dealer from "../helpers/dealer";
import Zone from '../helpers/zone';
import Text from '../helpers/text';
import Deck from '../helpers/#/deck';
import Rule from '../helpers/rule';
import Effect from '../helpers/effect';
import OppMove from '../helpers/oppmove.js';
import Action from '../helpers/action.js';
import Viewer from '../helpers/viewer.js';
import Operation from '../helpers/operation.js';
import Damage from '../helpers/damage.js';
import { Scene } from 'phaser';

export default class Game extends Phaser.Scene {
    constructor() {
        super({
            key: 'Game'
        });
    }


    preload() {
        this.load.setCORS('*');
        this.load.image('bg', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/bg.png');
        this.load.image('cyanCardBack', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/cardback1.png');
        this.load.image('magentaCardBack', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/pokemon-card-back.png');
        this.load.image('viewer', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/viewer.png');
        this.load.image('vstar', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/vstar.png');
        this.load.image('used', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/used.png');
        this.load.image('coinfront', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/coinfront.png');
        this.load.image('coinback', 'https://hkbgstor.blob.core.windows.net/ptcg-asset/coinback.png');
    }

    create() {
        this.serverUrl = process.env.SERVER_URL || 'http://localhost:3000';
        console.log(this.serverUrl);

        let background = this.add.image(960, 540, 'bg').setDisplaySize(1920, 1080).disableInteractive();
        background.depth = -100;

        this.zone = new Zone(this);
        this.text = new Text(this);
        this.deck = new Deck(this);
        this.dealer = new Dealer(this);
        this.rule = new Rule(this);
        this.action = new Action(this);
        this.oppMove = new OppMove(this);
        this.effect = new Effect(this);
        this.viewer = new Viewer(this);
        this.operation = new Operation(this);
        this.damage = new Damage(this);
        this.gameData = require('../helpers/gameData.json');
        this.diffJs = require('../lib/diff_match_patch.js');
        this.dmp = new this.diffJs.diff_match_patch();
        this.orgData = JSON.stringify(this.gameData);
        this.lastData = this.orgData;
        this.singleMode = false;
        this.patches = [];
        this.steps = [];
        this.history = [];
        this.users = [];
        this.me = {};
        this.cardImages = {
            "added": [],
            "loaded": []
        };
        this.gameStatus = {
            "userJoined": false,
            "imageLoaded": false,
            "init": false,
            "endFirst": "",
            "start": {
                "a": false,
                "b": false
            },
            "vstar": {
                "a": false,
                "b": false
            },
            "hand": {
                "a": 0,
                "b": 0
            },
            "activePkmStatus": {
                "a": [],
                "b": []
            }
        };
        this.turn = 0;
        this.yourTurn = true;
        this.chargedEnergy = false;
        this.playedFirst = false;
        this.viewCard = [];
        this.currentCard = {};
        this.player;
        this.opponent;
        this.sessionData;
        let self = this;

        this.socket = io(this.serverUrl);

        this.socket.on("connect", () => {
            console.log('Connected to server: ' + this.serverUrl);

            let query = window.location.search;
            var urlParams = new URLSearchParams(query);
            let cipher = urlParams.get('cipher');
            let token = urlParams.get('token') || undefined;
            let password = urlParams.get('password') || undefined;
            if (cipher) {
                let decryptedData = this.decryptCipher(cipher);
                let jsonData = JSON.stringify(decryptedData);
                sessionStorage.setItem(decryptedData.token, jsonData);
                console.log(`cipher: ${jsonData}`);
                // window.location.replace(`${window.location.href.split('?')[0]}?token=${decryptedData.token}`);
                let url;
                token = decryptedData.token
                if (decryptedData.password !== undefined) {
                    password = decryptedData.password
                    url = `${window.location.href.split('?')[0]}?token=${decryptedData.token}&password=${decryptedData.password}`
                } else {
                    url = `${window.location.href.split('?')[0]}?token=${decryptedData.token}`
                }
                window.history.replaceState({}, '', url)
                // return;
            }
            if (token) {
                let sessionStorageItem = sessionStorage.getItem(token);
                if (!sessionStorageItem) {
                    let tempURL = `/#/room?token=${token}`;
                    if (password !== undefined) {
                        tempURL = `${tempURL}&password=${password}`
                    }
                    window.location.replace(tempURL);
                    return;
                }
                self.sessionData = JSON.parse(sessionStorageItem);
                console.log(`sessionData.token: ${self?.sessionData?.token}`);
                console.log(`sessionData.name: ${self?.sessionData?.name}`);
                console.log(`sessionData.selectedDeck: ${self?.sessionData?.selectedDeck?.deckCode}`);
                console.log(`sessionData.singleMode: ${self?.sessionData?.singleMode}`);
                if (self.sessionData?.singleMode !== undefined) {
                    self.singleMode = self.sessionData.singleMode
                }
                self.me.name = self?.sessionData?.name;
                self.me.deckInfo = self?.sessionData?.selectedDeck;
                self.deck.loadDeck(self.me.deckInfo.deckCode)

                self.setName(self?.sessionData?.name);
            }
            this.socket.emit('join room', { token: token, password: password });
            this.socket.once('join room result', function (data) {
                if (data.result != 'success') {
                    // Join room failed
                    self.socket.disconnect();
                    self.socket = null;

                    window.alert(data.reason);
                    let tempURL = '/#/room';
                    window.location.replace(tempURL);
                    return;
                }

                //  Register success
                self.name = data.name;
                self.roomName = data.roomName;
                self.isPlayerA = data.isPlayerA;
                self.users = data.users;
                console.log(`isPlayerA: ${data.isPlayerA}`);
                console.log(data.users);
            });
        });
        this.socket.on('action', function (actionName, effect, obj, player) {
            console.log('player:' + self.player);
            console.log(self.gameData);
            self.action[actionName](effect, obj, player);
            self.steps.push([actionName, obj, player]);
            // if (self.gameStatus.start.a && self.gameStatus.start.b) {
            //     self.operation.relocateHand();
            //     // self.operation.relocateOppHand();
            // }

            self.notifyGameDataChanged();
        })

        this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
            gameObject.x = dragX;
            gameObject.y = dragY;
        })

        this.input.on('dragstart', function (pointer, gameObject) {
            // gameObject.setTint(0xff69b4);
            self.children.bringToTop(gameObject);
        })

        this.input.on('dragend', function (pointer, gameObject, dropped) {
            //effect testing
            // gameObject.cardData.effect.forEach(curEffect => {
            //     self.effect[curEffect](gameObject);
            // });
            if (!dropped) {
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;
            }
        })

        this.input.on('drop', function (pointer, gameObject, dropZone) {
            let move = true;
            let zoneName = dropZone.data.name.replace("self.", "");
            let start = self.gameStatus.start.a && self.gameStatus.start.b;
            if (self.singleMode && self.gameStatus.start.a) {
                start = true;
            }
            let action = self.rule.checkAction(gameObject, dropZone, self.gameData[self.player][zoneName], self.yourTurn, start);
            let emitAction = {};
            emitAction.gameObject = gameObject;
            emitAction.cardData = gameObject.cardData;

            if (!gameObject.currentZone || (gameObject.currentZone == "")) {
                for (let i = 0; i < self.gameData[self.player].hand.length; i++) {
                    if (self.gameData[self.player].hand[i].cardData == gameObject.cardData) {
                        self.gameData[self.player].hand.splice(i, 1);
                        gameObject.currentZone = "hand";
                    }
                }
            }

            emitAction.action = action;
            let effect;
            switch (action) {
                case 'playBattleFlip':
                case 'playBenchFlip':
                    emitAction.cardFront = gameObject.texture.key;
                    self.playedFirst = true;
                case 'playBattle':
                case 'playBench':
                    if (gameObject.currentZone == 'view') {
                        gameObject.x = gameObject.input.dragStartX;
                        gameObject.y = gameObject.input.dragStartY;
                    }
                    emitAction = self.effect.handToField(gameObject, dropZone, emitAction);
                    effect = 'handToField';
                    break;
                case 'retreatToBench':
                    emitAction = self.effect.swap(gameObject.currentZone, dropZone.data.name, emitAction);
                    effect = 'swap';
                    break;
                case 'chargeEnergy':
                    if (gameObject.currentZone == 'view') {
                        gameObject.x = gameObject.input.dragStartX;
                        gameObject.y = gameObject.input.dragStartY;
                    }
                    emitAction = self.effect.chargeEnergy(gameObject, dropZone, emitAction);
                    effect = 'chargeEnergy';
                    break;
                case 'attachTool':
                    emitAction = self.effect.attachTool(gameObject, dropZone, emitAction);
                    effect = 'attachTool';
                    break;
                case 'evolve':
                    emitAction = self.effect.evolve(gameObject, dropZone, emitAction);
                    effect = 'evolve';
                    break;
                case 'playTrainer':
                    emitAction = self.effect.playTrainer(gameObject, dropZone, emitAction);
                    effect = 'playTrainer';
                    break;
                case 'playStadium':
                    emitAction = self.effect.playStadium(gameObject, dropZone, emitAction);
                    effect = 'playStadium';
                    break;
                case 'benchToBattle':
                    emitAction = self.effect.benchToBattle(gameObject, dropZone, emitAction);
                    effect = 'benchToBattle';
                    break;
                case 'dropCard':
                    console.log(gameObject);
                    if (gameObject.currentZone == 'hand') {
                        self.effect.dropHand(gameObject);
                        emitAction = self.operation.buildEmitData(gameObject);
                        effect = 'dropHand';
                    }
                    else {
                        self.effect.dropField(gameObject);
                        emitAction = self.operation.buildEmitData(gameObject);
                        effect = 'dropField';
                    }
                    break;
                default:
                    gameObject.x = gameObject.input.dragStartX;
                    gameObject.y = gameObject.input.dragStartY;
                    // gameObject.currentZone = "";
                    move = false;
            }

            if (move) {
                self.viewer.allTextOff();
                self.socket.emit('action', 'oppMove', effect, emitAction, self.player);
            }

        })

    }

    update() {
        //single mode
        if (this.singleMode && this.me.deck && (this.cardImages.added.length == 0)) {
            this.me.deck.forEach(card => {
                if (card.Image) {
                    this.load.image(card.Image, card.Image);
                    this.cardImages.added.push(card.Image);
                }
            });
        }

        if ((this.singleMode) && !this.gameStatus.imageLoaded && (this.cardImages.added.length > 0)) {
            this.load.start();
            console.log('Image load ready: ' + this.load.progress);
            this.load.once('complete', () => {
                console.log('Load complete');
                this.turnText.setText(['  載入完成']);
                this.gameStatus.imageLoaded = true;
                this.startInit.setVisible(true);
            }, this);
        }
        //Load image
        if ((this.users.length == 2) && !this.gameStatus.imageLoaded && (this.cardImages.added.length >= 120)) {
            this.load.start();
            console.log('Image load ready: ' + this.load.progress);
            this.load.once('complete', () => {
                console.log('Load complete');
                this.turnText.setText(['  載入完成']);
                this.gameStatus.imageLoaded = true;
                this.startInit.setVisible(true);
            }, this);
        }

        if ((this.users.length == 2) && !this.gameStatus.userJoined) {
            console.log("load deck");
            this.socket.emit('action', 'syncPlayers', this.users, this.isPlayerA);
            // this.socket.emit('action', 'loadDeck', this.users, this.isPlayerA);
        }

        //control damage visibility
        if ((this.singleMode && this.gameStatus.start.a) || (this.gameStatus.start.a && this.gameStatus.start.b)) {
            if (parseInt(this.damage.selfBattleZone.text) > 0) {
                this.damage.selfBattleZoneCircle.visible = true;
                this.damage.selfBattleZone.visible = true;
            }
            else {
                this.damage.selfBattleZoneCircle.visible = false;
                this.damage.selfBattleZone.visible = false;
            }
            for (let i = 1; i <= 5; i++) {

                if (parseInt(this.damage[`selfBenchZone${i}`].text) > 0) {
                    this.damage[`selfBenchZone${i}Circle`].visible = true;
                    this.damage[`selfBenchZone${i}`].visible = true;
                }
                else {
                    this.damage[`selfBenchZone${i}Circle`].visible = false;
                    this.damage[`selfBenchZone${i}`].visible = false;
                }
            }
            if (parseInt(this.damage.oppBattleZone.text) > 0) {
                this.damage.oppBattleZoneCircle.visible = true;
                this.damage.oppBattleZone.visible = true;
            }
            else {
                this.damage.oppBattleZoneCircle.visible = false;
                this.damage.oppBattleZone.visible = false;
            }
            for (let i = 1; i <= 5; i++) {
                if (parseInt(this.damage[`oppBenchZone${i}`].text) > 0) {
                    this.damage[`oppBenchZone${i}Circle`].visible = true;
                    this.damage[`oppBenchZone${i}`].visible = true;
                }
                else {
                    this.damage[`oppBenchZone${i}Circle`].visible = false;
                    this.damage[`oppBenchZone${i}`].visible = false;
                }
            }
        }



        if ((this.singleMode && this.gameStatus.init) || (this.gameStatus.init && this.player && this.opponent)) {
            //#/deck and trash card real time update
            this.gameData.view.length == 0 ? this.text.viewZoneRegtangle.setVisible(false) : this.text.viewZoneRegtangle.setVisible(true);
            this.gameData[this.player].deck.length == 0 ? this.text.playerDeckLeftRegtangle.setVisible(false) : this.text.playerDeckLeftRegtangle.setVisible(true);
            this.gameData[this.player].graveyard.length == 0 ? this.text.playerGraveyardLeftRegtangle.setVisible(false) : this.text.playerGraveyardLeftRegtangle.setVisible(true);
            this.gameData[this.player].prize.length == 0 ? this.text.playerPrizeLeftRegtangle.setVisible(false) : this.text.playerPrizeLeftRegtangle.setVisible(true);
            this.gameData[this.player].lostZone.length == 0 ? this.text.playerLostZoneLeftRegtangle.setVisible(false) : this.text.playerLostZoneLeftRegtangle.setVisible(true);
            if (this.text.playerDeckLeft.text != this.gameData[this.player].deck.length) {
                this.text.playerDeckLeft.text = this.gameData[this.player].deck.length;
            }
            if (this.text.playerGraveyardLeft.text != this.gameData[this.player].graveyard.length) {
                this.text.playerGraveyardLeft.text = this.gameData[this.player].graveyard.length;
            }
            if (this.text.playerPrizeLeft.text != this.gameData[this.player].prize.length) {
                this.text.playerPrizeLeft.text = this.gameData[this.player].prize.length;
            }
            if (this.text.playerLostZoneLeft.text != this.gameData[this.player].lostZone.length) {
                this.text.playerLostZoneLeft.text = this.gameData[this.player].lostZone.length;
            }
            if (!this.singleMode) {
                this.gameData[this.opponent].deck.length == 0 ? this.text.oppDeckLeftRegtangle.setVisible(false) : this.text.oppDeckLeftRegtangle.setVisible(true);
                this.gameData[this.opponent].graveyard.length == 0 ? this.text.oppGraveyardLeftRegtangle.setVisible(false) : this.text.oppGraveyardLeftRegtangle.setVisible(true);
                this.gameData[this.opponent].prize.length == 0 ? this.text.oppPrizeLeftRegtangle.setVisible(false) : this.text.oppPrizeLeftRegtangle.setVisible(true);
                this.gameData[this.opponent].lostZone.length == 0 ? this.text.oppLostZoneLeftRegtangle.setVisible(false) : this.text.oppLostZoneLeftRegtangle.setVisible(true);
                if (this.text.oppDeckLeft.text != this.gameData[this.opponent].deck.length) {
                    this.text.oppDeckLeft.text = this.gameData[this.opponent].deck.length;
                }
                if (this.text.oppGraveyardLeft.text != this.gameData[this.opponent].graveyard.length) {
                    this.text.oppGraveyardLeft.text = this.gameData[this.opponent].graveyard.length;
                }
                if (this.text.oppPrizeLeft.text != this.gameData[this.opponent].prize.length) {
                    this.text.oppPrizeLeft.text = this.gameData[this.opponent].prize.length;
                }
                if (this.text.oppLostZoneLeft.text != this.gameData[this.opponent].lostZone.length) {
                    this.text.oppLostZoneLeft.text = this.gameData[this.opponent].lostZone.length;
                }
            }
            if (this.text.viewZone.text != this.gameData.view.length) {
                this.text.viewZone.text = this.gameData.view.length;
            }

            //real time relocate hand
            // if (this.gameData[this.player].hand.length != this.gameStatus.hand[this.player]) {

            //     this.operation.relocateHand();
            //     this.gameStatus.hand[this.player] = this.gameData[this.player].hand.length;
            // }
            // if (this.gameData[this.opponent].hand != this.gameStatus.hand[this.opponent]) {

            //     this.operation.relocateOppHand();
            //     this.gameStatus.hand[this.opponent] = this.gameData[this.opponent].hand.length;
            // }
        }




    }

    setName = (name) => {
        this.userName = name;
        this.socket.emit('set name', { 'name': name });
        console.log(`User:${this.socket.id} updated name to ${name}`);
    }

    decryptCipher = (ciphertext) => {
        ciphertext = ciphertext.replaceAll(' ', '+')
        let originalText = decodeURIComponent(escape(atob(ciphertext)));
        let jsonData = JSON.parse(originalText);
        return jsonData;
    }

    notifyGameDataChanged = () => {
        let data = this.gameData;
        this.createPatch(this.patches, this.lastData, data);
        this.lastData = JSON.stringify(data);
    }

    createPatch = (patches, lastData, data) => {
        let diff = this.dmp.diff_main(lastData, JSON.stringify(data));
        let patch = this.dmp.patch_make(lastData, diff);
        patches.push(patch);
        // console.log(JSON.stringify(patch));
    }

    patchData = (patches, orgData, maxStep) => {
        let patchedData = orgData;
        if (maxStep === undefined) {
            maxStep = patches.length;
        }
        for (let i = 0; i < patches.length && i < maxStep; i++) {
            let patch = patches[i];
            let patch_apply = this.dmp.patch_apply(patch, patchedData);
            patchedData = patch_apply[0];
        }

        return patchedData;
    }

    importPatches = (patchesJson) => {
        let input = JSON.parse(patchesJson);
        let patches = [];
        input.patchesText.forEach(patchText => {
            patches.push(this.dmp.patch_fromText(patchText));
        });
        this.orgData = input.orgData;
        this.patches = input.patches;
    }

    exportPatches = (patches, orgData) => {
        let patchesText = [];
        patches.forEach(patch => {
            patchesText.push(this.dmp.patch_toText(patch));
        });

        let output = {
            orgData: orgData,
            patchesText: patchesText,
        }

        return JSON.stringify(output);
    }

}