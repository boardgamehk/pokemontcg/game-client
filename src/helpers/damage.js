export default class Damage {
    constructor(scene) {
        let cardW = 150;
        let cardH = 210;
        let battleY = 570;
        let oppBattleY = 350;
        let benchStart = 100;
        let oppBenchY = 130;
        let benchY = 790;
        let gap = 50;
        let x = 40;
        let y = 65;
        let color = '#ffff00';
        let color2 = '#ffffff';
        let color3 = 0xff0000;

        //self
        this.selfBattleZone = scene.add.text(benchStart + 3*(cardW + gap) + x, battleY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.selfBattleZone.depth = 2;
        this.selfBattleZone.visible = false;
        this.selfBattleZoneCircle =  scene.add.circle(benchStart + 3*(cardW + gap) + x + 15, battleY - y + 10, 20, color3);
        this.selfBattleZoneCircle.depth = 1;
        this.selfBattleZoneCircle.visible = false;
        this.selfBattleZoneStatus = scene.add.text(benchStart + 3*(cardW + gap) - x - 75, battleY - y + 50, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        this.selfBattleZoneStatus.depth = 1;
        this.selfBattleZoneUsed = scene.add.image(benchStart + 3*(cardW + gap) - x, battleY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.selfBattleZoneUsed.depth = 1;
        this.selfBattleZoneUsed.visible = false;

        this.selfBenchZone1 = scene.add.text(benchStart + cardW + gap + x, benchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.selfBenchZone1.depth = 2;
        this.selfBenchZone1.visible = false;
        this.selfBenchZone1Circle =  scene.add.circle(benchStart + cardW + gap + x + 15, benchY - y + 10, 20, color3);
        this.selfBenchZone1Circle.depth = 1;
        this.selfBenchZone1Circle.visible = false;
        this.selfBenchZone1Used = scene.add.image(benchStart + cardW + gap + x - 80, benchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.selfBenchZone1Used.depth = 1;
        this.selfBenchZone1Used.visible = false;
        // this.selfBenchZone1Status = scene.add.text(benchStart + cardW + gap - x*1.5, benchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.selfBenchZone1Status.depth = 1;

        this.selfBenchZone2 = scene.add.text(benchStart + 2*(cardW + gap) + x, benchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.selfBenchZone2.depth = 2;
        this.selfBenchZone2.visible = false;
        this.selfBenchZone2Circle =  scene.add.circle(benchStart + 2*(cardW + gap) + x + 15, benchY - y + 10, 20, color3);
        this.selfBenchZone2Circle.depth = 1;
        this.selfBenchZone2Circle.visible = false;
        this.selfBenchZone2Used = scene.add.image(benchStart + 2*(cardW + gap) + x - 80, benchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.selfBenchZone2Used.depth = 1;
        this.selfBenchZone2Used.visible = false;
        // this.selfBenchZone2Status = scene.add.text(benchStart + 2*(cardW + gap) - x*1.5, benchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.selfBenchZone2Status.depth = 1;

        this.selfBenchZone3 = scene.add.text(benchStart + 3*(cardW + gap) + x, benchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.selfBenchZone3.depth = 2;
        this.selfBenchZone3.visible = false;
        this.selfBenchZone3Circle =  scene.add.circle(benchStart + 3*(cardW + gap) + x + 15, benchY - y + 10, 20, color3);
        this.selfBenchZone3Circle.depth = 1;
        this.selfBenchZone3Circle.visible = false;
        this.selfBenchZone3Used = scene.add.image(benchStart + 3*(cardW + gap) + x - 80, benchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.selfBenchZone3Used.depth = 1;
        this.selfBenchZone3Used.visible = false;
        // this.selfBenchZone3Status = scene.add.text(benchStart + 3*(cardW + gap) - x*1.5, benchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.selfBenchZone3Status.depth = 1;

        this.selfBenchZone4 = scene.add.text(benchStart + 4*(cardW + gap) + x, benchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.selfBenchZone4.depth = 2;
        this.selfBenchZone4.visible = false;
        this.selfBenchZone4Circle =  scene.add.circle(benchStart + 4*(cardW + gap) + x + 15, benchY - y + 10, 20, color3);
        this.selfBenchZone4Circle.depth = 1;
        this.selfBenchZone4Circle.visible = false;
        this.selfBenchZone4Used = scene.add.image(benchStart + 4*(cardW + gap) + x - 80, benchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.selfBenchZone4Used.depth = 1;
        this.selfBenchZone4Used.visible = false;
        // this.selfBenchZone4Status = scene.add.text(benchStart + 4*(cardW + gap) - x*1.5, benchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.selfBenchZone4Status.depth = 1;

        this.selfBenchZone5 = scene.add.text(benchStart + 5*(cardW + gap) + x, benchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.selfBenchZone5.depth = 2;
        this.selfBenchZone5.visible = false;
        this.selfBenchZone5Circle =  scene.add.circle(benchStart + 5*(cardW + gap) + x + 15, benchY - y + 10, 20, color3);
        this.selfBenchZone5Circle.depth = 1;
        this.selfBenchZone5Circle.visible = false;
        this.selfBenchZone5Used = scene.add.image(benchStart + 5*(cardW + gap) + x - 80, benchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.selfBenchZone5Used.depth = 1;
        this.selfBenchZone5Used.visible = false;
        // this.selfBenchZone5Status = scene.add.text(benchStart + 5*(cardW + gap) - x*1.5, benchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.selfBenchZone5Status.depth = 1;

        //opp

        this.oppBattleZone = scene.add.text(benchStart + 3*(cardW + gap) + x, oppBattleY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.oppBattleZone.depth = 2;
        this.oppBattleZone.visible = false;
        this.oppBattleZoneCircle =  scene.add.circle(benchStart + 3*(cardW + gap) + x + 15, oppBattleY - y + 10, 20, color3);
        this.oppBattleZoneCircle.depth = 1;
        this.oppBattleZoneCircle.visible = false;
        this.oppBattleZoneStatus = scene.add.text(benchStart + 3*(cardW + gap) - x - 75, oppBattleY - y - 20, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        this.oppBattleZoneStatus.depth = 1;
        this.oppBattleZoneUsed = scene.add.image(benchStart + 3*(cardW + gap) - x, oppBattleY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.oppBattleZoneUsed.depth = 1;
        this.oppBattleZoneUsed.visible = false;

        this.oppBenchZone1 = scene.add.text(benchStart + cardW + gap + x, oppBenchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.oppBenchZone1.depth = 2;
        this.oppBenchZone1.visible = false;
        this.oppBenchZone1Circle =  scene.add.circle(benchStart + cardW + gap + x + 15, oppBenchY - y + 10, 20, color3);
        this.oppBenchZone1Circle.depth = 1;
        this.oppBenchZone1Circle.visible = false;
        this.oppBenchZone1Used = scene.add.image(benchStart + cardW + gap + x - 80, oppBenchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.oppBenchZone1Used.depth = 1;
        this.oppBenchZone1Used.visible = false;
        // this.oppBenchZone1Status = scene.add.text(benchStart + cardW + gap - x*1.5, oppBenchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.oppBenchZone1Status.depth = 1;

        this.oppBenchZone2 = scene.add.text(benchStart + 2*(cardW + gap) + x, oppBenchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.oppBenchZone2.depth = 2;
        this.oppBenchZone2.visible = false;
        this.oppBenchZone2Circle =  scene.add.circle(benchStart + 2*(cardW + gap) + x + 15, oppBenchY - y + 10, 20, color3);
        this.oppBenchZone2Circle.depth = 1;
        this.oppBenchZone2Circle.visible = false;
        this.oppBenchZone2Used = scene.add.image(benchStart + 2*(cardW + gap) + x - 80, oppBenchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.oppBenchZone2Used.depth = 1;
        this.oppBenchZone2Used.visible = false;
        // this.oppBenchZone2Status = scene.add.text(benchStart + 2*(cardW + gap) - x*1.5, oppBenchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.oppBenchZone2Status.depth = 1;

        this.oppBenchZone3 = scene.add.text(benchStart + 3*(cardW + gap) + x, oppBenchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.oppBenchZone3.depth = 2;
        this.oppBenchZone3.visible = false;
        this.oppBenchZone3Circle =  scene.add.circle(benchStart + 3*(cardW + gap) + x + 15, oppBenchY - y + 10, 20, color3);
        this.oppBenchZone3Circle.depth = 1;
        this.oppBenchZone3Circle.visible = false;
        this.oppBenchZone3Used = scene.add.image(benchStart + 3*(cardW + gap) + x - 80, oppBenchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.oppBenchZone3Used.depth = 1;
        this.oppBenchZone3Used.visible = false;
        // this.oppBenchZone3Status = scene.add.text(benchStart + 3*(cardW + gap) - x*1.5, oppBenchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.oppBenchZone3Status.depth = 1;

        this.oppBenchZone4 = scene.add.text(benchStart + 4*(cardW + gap) + x, oppBenchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.oppBenchZone4.depth = 2;
        this.oppBenchZone4.visible = false;
        this.oppBenchZone4Circle =  scene.add.circle(benchStart + 4*(cardW + gap) + x + 15, oppBenchY - y + 10, 20, color3);
        this.oppBenchZone4Circle.depth = 1;
        this.oppBenchZone4Circle.visible = false;
        this.oppBenchZone4Used = scene.add.image(benchStart + 4*(cardW + gap) + x - 80, oppBenchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.oppBenchZone4Used.depth = 1;
        this.oppBenchZone4Used.visible = false;
        // this.oppBenchZone4Status = scene.add.text(benchStart + 4*(cardW + gap) - x*1.5, oppBenchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.oppBenchZone4Status.depth = 1;

        this.oppBenchZone5 = scene.add.text(benchStart + 5*(cardW + gap) + x, oppBenchY - y, [0]).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color);
        this.oppBenchZone5.depth = 2;
        this.oppBenchZone5.visible = false;
        this.oppBenchZone5Circle =  scene.add.circle(benchStart + 5*(cardW + gap) + x + 15, oppBenchY - y + 10, 20, color3);
        this.oppBenchZone5Circle.depth = 1;
        this.oppBenchZone5Circle.visible = false;
        this.oppBenchZone5Used = scene.add.image(benchStart + 5*(cardW + gap) + x - 80, oppBenchY - y + 15, 'used').setDisplaySize(60, 60).disableInteractive();
        this.oppBenchZone5Used.depth = 1;
        this.oppBenchZone5Used.visible = false;
        // this.oppBenchZone5Status = scene.add.text(benchStart + 5*(cardW + gap) - x*1.5, oppBenchY - y, ['']).setFontSize(18).setFontFamily('Trebuchet MS').setColor(color2);
        // this.oppBenchZone5Status.depth = 1;

        this.stadiumUsed = scene.add.image(benchStart + 2*(cardW + gap) + x - 80, 420, 'used').setDisplaySize(60, 60).disableInteractive();
        this.stadiumUsed.depth = 1;
        this.stadiumUsed.visible = false;

        this.abilityRegtangle = scene.add.rectangle(0, 0, 120, 60, 0x394ba0);
        this.abilityRegtangle.depth = 1;
        this.abilityRegtangle.visible = false;
        this.abilityText = scene.add.text(0, 0, ['']).setFontSize(48).setFontFamily('Trebuchet MS').setColor(color);
        this.abilityText.depth = 2;
        this.abilityText.visible = false;


    }
}
