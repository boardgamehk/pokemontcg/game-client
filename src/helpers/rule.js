export default class Rule {
    constructor(scene) {
        this.checkAction = (obj, dropZone, gameDataZone, yourTurn, canStart) => {
            console.log(canStart)
            if ((obj.currentZone != 'view') && obj.cardData && dropZone.data.name == "self.graveyard") {
                return "dropCard"
            }
            if (yourTurn && obj.cardData.Category) {
                if ((obj.cardData.Category == "pokemon") && (dropZone.data.name != "trainer")) {
                    if ((obj.currentZone == 'hand') || (obj.currentZone == 'view')) {
                        if ((dropZone.data.name == "self.battleZone") && !gameDataZone.mainCard.cardData) {
                            if (canStart) {
                                return "playBattle"
                            }
                            else {
                                return "playBattleFlip"
                            }
                            
                        }
                        if ((dropZone.data.name != "self.battleZone") && (dropZone.data.name != "view") && !gameDataZone.mainCard.cardData) {
                            for (let i=1;i<=5;i++) {
                                if ((dropZone.data.name == "self.benchZone" + i) && !gameDataZone.mainCard.cardData) {
                                    if (canStart) {
                                        return "playBench"
                                    }
                                    if (scene.playedFirst && !canStart) {
                                        return "playBenchFlip"
                                    }
                                    
                                }
                            }
                        }
                    }
                    else {
                        if (obj.currentZone == "battleZone") {
                            for (let i=1;i<=5;i++) {
                                let zoneName = "self.benchZone" + i;
                                if ((dropZone.data.name == zoneName) && gameDataZone.mainCard.cardData) {
    
                                    return "retreatToBench"
                                }
                            }
                        }
                    }
                }
                
                if (canStart) {
                    for (let i=1;i<=5;i++) {
                        if (obj.currentZone == "benchZone" + i) {
                            if ((dropZone.data.name == 'self.battleZone') && !gameDataZone.mainCard.cardData) {
                                return "benchToBattle";
                            } 
                        }
                    }
                    if ((obj.currentZone == 'hand') || (obj.currentZone == 'view')) {
                        if (((obj.cardData.Category == "item") || (obj.cardData.Category == "supporter")) && (dropZone.data.name == "trainer")) {
                            return "playTrainer"
                        }
                        if ((obj.cardData.Category == "stadium") && (dropZone.data.name == "stadium")) {
                            return "playStadium"
                        }
                        if (gameDataZone && gameDataZone.mainCard) {
                            if ((obj.cardData.Category == "energy")) {
                                if (gameDataZone.mainCard.cardData && !scene.chargedEnergy) {
                                    // scene.chargedEnergy = true;
                                    return "chargeEnergy"
                                } 
                            }
                            if ((obj.cardData.Category == "tool")) {
                                return "attachTool"
                            }
                            if ((obj.cardData.Category == "pokemon") && (gameDataZone.cardStack.pokemon.length < 2)) {
                                return "evolve"
                            }
                        }
                    }
                }
            }

            return "invalid"

        }
    }
}