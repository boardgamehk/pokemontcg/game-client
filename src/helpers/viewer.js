import Card from './card'

export default class Viewer {
    constructor(scene) {


        this.cardList = [];
        this.closeText = scene.text.renderText('[關閉]', 1320, 5);
        this.closeText.setColor('#ffffff');
        this.closeText.setFontSize(30);
        this.closeText.on('pointerover', function () {
            scene.viewer.closeText.setColor('#c0c0c0');
        })
        this.closeText.on('pointerout', function () {
            scene.viewer.closeText.setColor('#ffffff');
        })
        this.closeText.depth = 12;
        this.closeText.setVisible(false);
        this.closeText.on('pointerdown', function () {
            scene.viewer.cardList.forEach(element => {
                element.destroy();
            });
            scene.viewer.cardList = [];
            scene.viewer.bg.visible = false;
            scene.viewer.closeText.setVisible(false);
            scene.viewer.allToDeck.setVisible(false);
            scene.viewer.allToGraveyard.setVisible(false);
        });


        this.allToDeck = scene.add.text(20, 5, ['[全部回牌組]']).setFontSize(30).setFontFamily('Trebuchet MS').setColor('#ffffff').setInteractive();
        this.allToDeck.depth = 12;
        this.allToDeck.setVisible(false);
        this.allToDeck.on('pointerover', function () {
            scene.viewer.allToDeck.setColor('#c0c0c0');
        });
        this.allToDeck.on('pointerout', function () {
            scene.viewer.allToDeck.setColor('#ffffff');
        });
        this.allToDeck.on('pointerdown', function () {
            let length = scene.viewer.cardList.length;
            for (let i=0;i<length;i++) {
                if (scene.viewer.cardList[0].currentZone == 'view') {
                    scene.socket.emit('action', 'oppMove', 'viewToDeck', scene.operation.buildEmitData(scene.viewer.cardList[0]), scene.player);
                    scene.effect.viewToDeck(scene.viewer.cardList[0]);
                    scene.operation.shuffleDeck(scene.gameData[scene.player].deck);
                } 
                else if (scene.viewer.cardList[0].currentZone == 'graveyard') {
                    scene.socket.emit('action', 'oppMove', 'gravyardToDeck', scene.operation.buildEmitData(scene.viewer.cardList[0]), scene.player);
                    scene.effect.gravyardToDeck(scene.viewer.cardList[0]);
                    scene.operation.shuffleDeck(scene.gameData[scene.player].deck);
                }
                else {
                    scene.viewer.cardList[0].destroy();
                    scene.viewer.cardList.splice(0, 1);
                }
            }
            scene.viewer.cardList = [];
            scene.viewer.bg.visible = false;
            scene.viewer.closeText.setVisible(false);
            scene.viewer.allToDeck.setVisible(false);
            scene.viewer.allToGraveyard.setVisible(false);
        });

        this.allToGraveyard = scene.add.text(230, 5, ['[全部棄牌]']).setFontSize(30).setFontFamily('Trebuchet MS').setColor('#ffffff').setInteractive();
        this.allToGraveyard.depth = 12;
        this.allToGraveyard.setVisible(false);
        this.allToGraveyard.on('pointerover', function () {
            scene.viewer.allToGraveyard.setColor('#c0c0c0');
        });
        this.allToGraveyard.on('pointerout', function () {
            scene.viewer.allToGraveyard.setColor('#ffffff');
        });
        this.allToGraveyard.on('pointerdown', function () {
            let length = scene.viewer.cardList.length;
            for (let i=0;i<length;i++) {
                if (scene.viewer.cardList[0].currentZone == 'view') {
                    scene.socket.emit('action', 'oppMove', 'dropView', scene.operation.buildEmitData(scene.viewer.cardList[0]), scene.player);
                    scene.effect.dropView(scene.viewer.cardList[0]);
                } 
                else if (scene.viewer.cardList[0].currentZone == 'deck') {
                    scene.socket.emit('action', 'oppMove', 'dropDeck', scene.operation.buildEmitData(scene.viewer.cardList[0]), scene.player);
                    scene.effect.dropDeck(scene.viewer.cardList[0]);
                } 
                else {
                    scene.viewer.cardList[0].destroy();
                    scene.viewer.cardList.splice(0, 1);
                }
            }
            scene.viewer.cardList = [];
            scene.viewer.bg.visible = false;
            scene.viewer.closeText.setVisible(false);
            scene.viewer.allToDeck.setVisible(false);
            scene.viewer.allToGraveyard.setVisible(false);
        });

        this.bg = scene.add.image(715, 540, 'viewer').setDisplaySize(1430, 1080);
        this.bg.depth = 10;
        this.bg.visible = false;

        this.allTextOff = (keepDamageText) => {
            if (!keepDamageText) {
                scene.poison.setVisible(false);
                scene.burn.setVisible(false);
                scene.paralysis.setVisible(false);
                scene.sleep.setVisible(false);
                scene.add100.setVisible(false);
                scene.add10.setVisible(false);
                scene.deduct100.setVisible(false);
                scene.deduct10.setVisible(false);
                scene.ability.setVisible(false);
                scene.attack1.setVisible(false);
                scene.attack2.setVisible(false);
                scene.target.setVisible(false);
                scene.backToDeckTop.setVisible(false);
                scene.backToDeckBottom.setVisible(false);
                scene.backToHand.setVisible(false);
                scene.dropToGraveyard.setVisible(false);
                scene.dropToLost.setVisible(false);
                scene.toPrize.setVisible(false);
                scene.showCard.setVisible(false);
                scene.toView.setVisible(false);
                scene.flipPrize.setVisible(false);
            }
            // scene.backToDeckTop.setVisible(false);
            // scene.backToDeckBottom.setVisible(false);
            // scene.backToHand.setVisible(false);
            // scene.dropToGraveyard.setVisible(false);
            // scene.dropToLost.setVisible(false);
            // scene.toPrize.setVisible(false);
            // scene.showCard.setVisible(false);
            // scene.toView.setVisible(false);
        }

        this.setCardView = (card, index) => {
            card.on('pointerdown', function () {
                let view = new Card(scene);
                let viewCard = view.render(1685, 400, card.texture.key).setDisplaySize(440, 616);
                viewCard.on('pointerdown', function () {
                    window.open(card.texture.key);
                });
                scene.viewer.allTextOff();
                scene.showOppCard.visible = false;
                scene.viewCard.push(viewCard);
                if (scene.viewCard.length > 1) {
                    scene.viewCard[0].destroy();
                    scene.viewCard.pop();
                }
                scene.viewCard[0].cardData = card.cardData;
                scene.viewCard[0].currentZone = card.currentZone;
                scene.viewCard[0].isMain = card.isMain;
                scene.viewCard[0].index = index;
                scene.viewer.applyAction(scene.viewCard[0]);
            });
        }

        this.applyAction = (card) => {
            scene.viewer.allTextOff();
            console.log(card);
            if ((scene.singleMode && scene.gameStatus.start.a) || (scene.gameStatus.start.a && scene.gameStatus.start.b)) {
                if (card.cardData.owner == scene.player) {
                    if (scene.yourTurn) {
                        scene.ability.setText(['特性']);
                        switch (scene.operation.removeZoneStr(card.currentZone)) {
                            case ('stadium'):
                                scene.target.setVisible(true);
                                scene.ability.setVisible(true);
                                scene.ability.setText(['發動']);
                                scene.dropToLost.setVisible(true);
                                break;
                            case ('trainer'):
                                scene.backToDeckTop.setVisible(true);
                                scene.backToDeckBottom.setVisible(true);
                                scene.backToHand.setVisible(true);
                                scene.dropToGraveyard.setVisible(true);
                                scene.dropToLost.setVisible(true);
                                scene.toPrize.setVisible(true);
                                break;
                            case ('prize'):
                                scene.backToHand.setVisible(true);
                                scene.showCard.setVisible(true);
                                scene.flipPrize.setVisible(true);
                                break;
                            case ('battleZone'):
                                if (card.isMain) {
                                    scene.attack1.setVisible(true);
                                    scene.attack2.setVisible(true);
                                    scene.poison.setVisible(true);
                                    scene.burn.setVisible(true);
                                    scene.paralysis.setVisible(true);
                                    scene.sleep.setVisible(true);
                                }
                            case ('benchZone1'):
                            case ('benchZone2'):
                            case ('benchZone3'):
                            case ('benchZone4'):
                            case ('benchZone5'):
                                if (card.isMain) {
                                    scene.ability.setVisible(true);
                                    scene.add100.setVisible(true);
                                    scene.add10.setVisible(true);
                                    scene.deduct100.setVisible(true);
                                    scene.deduct10.setVisible(true);
                                }
                            default:
                                scene.backToDeckTop.setVisible(true);
                                scene.backToDeckBottom.setVisible(true);
                                scene.backToHand.setVisible(true);
                                scene.dropToGraveyard.setVisible(true);
                                scene.dropToLost.setVisible(true);
                                // scene.toPrize.setVisible(true);
                                scene.showCard.setVisible(true);
                                if ((card.cardData.Category == 'pokemon') || (card.cardData.Category == 'energy')) {
                                    scene.toView.setVisible(true);
                                }
                        }
                    }
                    else {
                        scene.backToDeckTop.setVisible(true);
                        scene.backToDeckBottom.setVisible(true);
                        scene.backToHand.setVisible(true);
                        scene.dropToGraveyard.setVisible(true);
                        scene.dropToLost.setVisible(true);
                        scene.showCard.setVisible(true);
                    }
                }
                else {
                    scene.target.setVisible(true);
                }

            }

        }

        this.viewAll = (zone) => {
            this.closeText.setVisible(true);
            scene.viewer.allToDeck.setVisible(true);
            //scene.viewer.allToGraveyard.setVisible(true);
            this.bg.visible = true;
            let x = 120;
            let y = 150;
            if (zone == 'view') {
                scene.gameData.view.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, element.texture.key).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'view';
                    scene.viewer.setCardView(image);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
            if (zone == 'graveyard') {
                scene.gameData[scene.player].graveyard.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, element.texture.key).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'graveyard';
                    scene.viewer.setCardView(image);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
            if (zone == 'oppGraveyard') {
                scene.gameData[scene.opponent].graveyard.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, element.texture.key).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'opp.graveyard';
                    scene.viewer.setCardView(image);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
            if (zone == 'lostZone') {
                scene.gameData[scene.player].lostZone.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, element.texture.key).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'lostZone';
                    scene.viewer.setCardView(image);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
            if (zone == 'oppLostZone') {
                scene.gameData[scene.opponent].lostZone.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, element.texture.key).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'opp.lostZone';
                    scene.viewer.setCardView(image);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
            if (zone == 'deck') {
                scene.gameData[scene.player].deck.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, element.cardData.Image).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'deck';
                    scene.viewer.setCardView(image);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
            if (zone == 'prize') {
                scene.gameData[scene.player].prize = scene.operation.shuffleDeck(scene.gameData[scene.player].prize);
                scene.gameData[scene.player].prize.forEach((element, index, object) => {
                    if (x > 1400) {
                        x = 120;
                        y += 200;
                    }
                    let image = scene.add.image(x, y, scene.operation.playerSprite()).setDisplaySize(150, 210).setInteractive();
                    image.depth = 11;
                    image.cardData = element.cardData;
                    image.currentZone = 'prize';
                    scene.viewer.setCardView(image, index);
                    scene.viewer.cardList.push(image);
                    x+=150;
                });
                // this.setViewable(scene.viewer.cardList);
            }
        }
    }
}