export default class Text {
    constructor(scene) {

        this.renderText = (text, x, y) => {
            let addedText = scene.add.text(x, y, [text]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#c0c0c0').setInteractive();
            addedText.on('pointerover', function () {
                addedText.setColor('#ffffff');
            })
            
            addedText.on('pointerout', function () {
                addedText.setColor('#c0c0c0');
            })
            
            

            return addedText;
        };

        scene.drawText = this.renderText('開始', 1600, 800);
        scene.drawText.setFontSize(72);
        scene.drawText.setVisible(false);
        scene.drawText.disableInteractive();
        scene.drawText.on('pointerdown', function () {
            if (Object.keys(scene.gameData[scene.player].battleZone.mainCard).length > 0) {
                console.log("Ready");
                scene.gameStatus.start[scene.player] = true;
                scene.socket.emit('action', 'startGame', scene.gameStatus.start, scene.isPlayerA);
            }
            // if ((Object.keys(scene.gameData.a.battleZone.mainCard).length > 0) && (Object.keys(scene.gameData.b.battleZone.mainCard).length > 0)) {
            //     console.log("Ready");
            //     scene.gameStatus.start[scene.player] = true;
            //     scene.socket.emit('action', 'startGame', scene.gameStatus.start, this.isPlayerA);
            // }
        })
        scene.endText = this.renderText('  結束回合', 1550, 20);
        scene.endText.setFontSize(52);
        scene.endText.setVisible(false);
        scene.endText.disableInteractive();
        scene.endText.on('pointerdown', function () {
            let player = scene.player;
            scene.socket.emit("action", "endTurn", player, player);
        })
        scene.turnText = this.renderText('等待對手中', 1550, 20);
        scene.turnText.setFontSize(52);
        scene.turnText.setColor('#ffffff');
        scene.turnText.disableInteractive();
        // scene.historyText = this.renderText('歷史記錄', 1238, 320);
        // scene.historyText.setFontSize(32);
        // scene.historyText.on('pointerdown', function () {
        //     window.alert(scene.history);
        // })

        scene.changeToSingle = this.renderText('  單人模式', 1550, 820);
        scene.changeToSingle.setFontSize(52);
        scene.changeToSingle.setColor('#ffffff');
        scene.changeToSingle.on('pointerdown', function () {
            scene.singleMode = true;
            scene.changeToSingle.setVisible(false);
        });

        let fontSize = 36;
        let startX = 1480;
        let startY = 770
        let xGap = 150;
        let yGap = 50;

        scene.startInit = this.renderText('開始對戰(先攻)', 1490, 500);
        scene.startInit.setFontSize(60);
        scene.startInit.on('pointerdown', function () {
            scene.socket.emit('action', 'initGame', this.users, scene.player);
        });
        scene.startInit.setVisible(false);

        scene.target = this.renderText('指定', startX + xGap, startY - 50);
        scene.target.setFontSize(fontSize);
        scene.target.on('pointerdown', function () {
            scene.effect.targetCard(scene.viewCard[0]);
            scene.socket.emit('action', 'oppMove', 'targetCard', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
        });
        scene.target.setVisible(false);

        scene.ability = this.renderText('特性', startX, startY - 50);
        scene.ability.setFontSize(fontSize);
        scene.ability.on('pointerdown', function () {
            scene.effect.showUsedAbility(scene.viewCard[0]);
            scene.socket.emit('action', 'oppMove', 'showUsedAbility', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.ability.setVisible(false);

        scene.attack1 = this.renderText('一技', startX + xGap, startY - 50);
        scene.attack1.setFontSize(fontSize);
        scene.attack1.on('pointerdown', function () {
            scene.effect.showAttackText(scene.viewCard[0], '一技');
            scene.socket.emit('action', 'oppMove', 'showAttackText', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.attack1.setVisible(false);

        scene.attack2 = this.renderText('二技', startX + 300, startY - 50);
        scene.attack2.setFontSize(fontSize);
        scene.attack2.on('pointerdown', function () {
            scene.effect.showAttackText(scene.viewCard[0], '二技');
            scene.socket.emit('action', 'oppMove', 'showAttackText', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.attack2.setVisible(false);

        scene.add100 = this.renderText('+100', startX, startY);
        scene.add100.setFontSize(fontSize);
        scene.add100.on('pointerdown', function () {
            scene.effect.damage(scene.viewCard[0], 100);
            let obj = {};
            obj.card = scene.operation.buildEmitData(scene.viewCard[0], false);
            obj.damage = 100;
            scene.socket.emit('action', 'oppMove', 'damage', obj, scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.add100.setVisible(false);

        scene.add10 = this.renderText('+10', startX + xGap, startY);
        scene.add10.setFontSize(fontSize);
        scene.add10.on('pointerdown', function () {
            scene.effect.damage(scene.viewCard[0], 10);
            let obj = {};
            obj.card = scene.operation.buildEmitData(scene.viewCard[0], false);
            obj.damage = 10;
            scene.socket.emit('action', 'oppMove', 'damage', obj, scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.add10.setVisible(false);

        scene.deduct100 = this.renderText('-100', startX, startY + yGap);
        scene.deduct100.setFontSize(fontSize);
        scene.deduct100.on('pointerdown', function () {
            scene.effect.damage(scene.viewCard[0], -100);
            let obj = {};
            obj.card = scene.operation.buildEmitData(scene.viewCard[0], false);
            obj.damage = -100;
            scene.socket.emit('action', 'oppMove', 'damage', obj, scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.deduct100.setVisible(false);

        scene.deduct10 = this.renderText('-10', startX + xGap, startY + yGap);
        scene.deduct10.setFontSize(fontSize);
        scene.deduct10.on('pointerdown', function () {
            scene.effect.damage(scene.viewCard[0], -10);
            let obj = {};
            obj.card = scene.operation.buildEmitData(scene.viewCard[0], false);
            obj.damage = -10;
            scene.socket.emit('action', 'oppMove', 'damage', obj, scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.deduct10.setVisible(false);

        scene.poison = this.renderText('中毒', startX, startY + yGap*2);
        scene.poison.setFontSize(fontSize);
        scene.poison.on('pointerdown', function () {
            scene.effect.setStatus(scene.viewCard[0], '中毒');
            // let owner = scene.viewCard[0].cardData.owner;
            // let zone = scene.viewCard[0].currentZone;
            // let index = scene.gameData[owner][zone].mainCard.cardData.status.indexOf('中毒');
            // if (index !== -1) {
            //     scene.gameData[owner][zone].mainCard.cardData.status.splice(index, 1);
            // }
            // else {
            //     scene.gameData[owner][zone].mainCard.cardData.status.push('中毒');
            // }
            scene.socket.emit('action', 'oppMove', 'setStatus', '中毒', scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.poison.setVisible(false);

        scene.burn = this.renderText('燒傷', startX + xGap, startY + yGap*2);
        scene.burn.setFontSize(fontSize);
        scene.burn.on('pointerdown', function () {
            scene.effect.setStatus(scene.viewCard[0], '燒傷');
            // let owner = scene.viewCard[0].cardData.owner;
            // let zone = scene.viewCard[0].currentZone;
            // let index = scene.gameData[owner][zone].mainCard.cardData.status.indexOf('燒傷');
            // if (index !== -1) {
            //     scene.gameData[owner][zone].mainCard.cardData.status.splice(index, 1);
            // }
            // else {
            //     scene.gameData[owner][zone].mainCard.cardData.status.push('燒傷');
            // }
            scene.socket.emit('action', 'oppMove', 'setStatus', '燒傷', scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.burn.setVisible(false);

        scene.paralysis = this.renderText('麻痹', startX, startY + yGap*3);
        scene.paralysis.setFontSize(fontSize);
        scene.paralysis.on('pointerdown', function () {
            scene.effect.setStatus(scene.viewCard[0], '麻痹');
            // let owner = scene.viewCard[0].cardData.owner;
            // let zone = scene.viewCard[0].currentZone;
            // let index = scene.gameData[owner][zone].mainCard.cardData.status.indexOf('麻痹');
            // if (index !== -1) {
            //     scene.gameData[owner][zone].mainCard.cardData.status.splice(index, 1);
            // }
            // else {
            //     scene.gameData[owner][zone].mainCard.cardData.status.push('麻痹');
            // }
            scene.socket.emit('action', 'oppMove', 'setStatus', '麻痹', scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.paralysis.setVisible(false);

        scene.sleep = this.renderText('睡眠', startX + xGap, startY + yGap*3);
        scene.sleep.setFontSize(fontSize);
        scene.sleep.on('pointerdown', function () {
            scene.effect.setStatus(scene.viewCard[0], '睡眠');
            // let owner = scene.viewCard[0].cardData.owner;
            // let zone = scene.viewCard[0].currentZone;
            // let index = scene.gameData[owner][zone].mainCard.cardData.status.indexOf('睡眠');
            // if (index !== -1) {
            //     scene.gameData[owner][zone].mainCard.cardData.status.splice(index, 1);
            // }
            // else {
            //     scene.gameData[owner][zone].mainCard.cardData.status.push('睡眠');
            // }
            scene.socket.emit('action', 'oppMove', 'setStatus', '睡眠', scene.player);
            scene.viewer.allTextOff(true);
        });
        scene.sleep.setVisible(false);

        scene.backToDeckTop = this.renderText('回牌面', startX, startY + yGap*4);
        scene.backToDeckTop.setFontSize(fontSize);
        scene.backToDeckTop.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'trainer':
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                    scene.socket.emit('action', 'oppMove', 'fieldToDeck', scene.operation.buildEmitData(scene.viewCard[0], true), scene.player);
                    scene.effect.fieldToDeck(scene.viewCard[0], true);
                    break;
                case 'graveyard':
                    scene.socket.emit('action', 'oppMove', 'fieldToDeck', scene.operation.buildEmitData(scene.viewCard[0], true), scene.player);
                    scene.effect.gravyardToDeck(scene.viewCard[0], true);
                    break;
                case 'hand':
                    scene.socket.emit('action', 'oppMove', 'handToDeck', scene.operation.buildEmitData(scene.viewCard[0], true), scene.player);
                    scene.effect.handToDeck(scene.viewCard[0], true);
                    break;
                case 'view':
                    scene.socket.emit('action', 'oppMove', 'viewToDeck', scene.operation.buildEmitData(scene.viewCard[0], true), scene.player);
                    scene.effect.viewToDeck(scene.viewCard[0], true);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.backToDeckTop.setVisible(false);

        scene.backToDeckBottom = this.renderText('回牌底', startX + xGap, startY + yGap*4);
        scene.backToDeckBottom.setFontSize(fontSize);
        scene.backToDeckBottom.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'trainer':
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                    scene.socket.emit('action', 'oppMove', 'fieldToDeck', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
                    scene.effect.fieldToDeck(scene.viewCard[0], false);
                    break;
                case 'graveyard':
                    scene.socket.emit('action', 'oppMove', 'gravyardToDeck', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
                    scene.effect.gravyardToDeck(scene.viewCard[0], false);
                    break;
                case 'hand':
                    scene.socket.emit('action', 'oppMove', 'handToDeck', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
                    scene.effect.handToDeck(scene.viewCard[0], false);
                    break;
                case 'view':
                    scene.socket.emit('action', 'oppMove', 'viewToDeck', scene.operation.buildEmitData(scene.viewCard[0], false), scene.player);
                    scene.effect.viewToDeck(scene.viewCard[0], false);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.backToDeckBottom.setVisible(false);

        scene.toPrize = this.renderText('放獎賞', startX + xGap*2, startY + yGap*3);
        scene.toPrize.setFontSize(fontSize);
        scene.toPrize.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'hand':
                    scene.socket.emit('action', 'oppMove', 'handToPrize', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.handToPrize(scene.viewCard[0]);
                    break;
                case 'trainer':
                    scene.socket.emit('action', 'oppMove', 'fieldToPrize', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.fieldToPrize(scene.viewCard[0]);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.toPrize.setVisible(false);

        scene.toView = this.renderText('上場', startX + xGap*2, startY + yGap*4);
        scene.toView.setFontSize(fontSize);
        scene.toView.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'graveyard':
                    scene.socket.emit('action', 'oppMove', 'gravyardToView', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.gravyardToView(scene.viewCard[0]);
                    break;
                case 'deck':
                    scene.socket.emit('action', 'oppMove', 'deckToView', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.deckToView(scene.viewCard[0]);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.toView.setVisible(false);

        scene.backToHand = this.renderText('回手', startX, startY + yGap*5);
        scene.backToHand.setFontSize(fontSize);
        scene.backToHand.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'trainer':
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                    scene.socket.emit('action', 'oppMove', 'fieldToHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.fieldToHand(scene.viewCard[0]);
                    break;
                case 'deck':
                    scene.socket.emit('action', 'oppMove', 'deckToHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.deckToHand(scene.viewCard[0]);
                    break;
                case 'graveyard':
                    scene.socket.emit('action', 'oppMove', 'gravyardToHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.gravyardToHand(scene.viewCard[0]);
                    break;
                case 'view':
                    scene.socket.emit('action', 'oppMove', 'viewToHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.viewToHand(scene.viewCard[0]);
                    break;
                case 'prize':
                    scene.socket.emit('action', 'oppMove', 'prizeToHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.prizeToHand(scene.viewCard[0]);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.backToHand.setVisible(false);

        scene.dropToGraveyard = this.renderText('棄牌', startX + xGap, startY + yGap*5);
        scene.dropToGraveyard.setFontSize(fontSize);
        scene.dropToGraveyard.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'trainer':
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                    scene.socket.emit('action', 'oppMove', 'dropField', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.dropField(scene.viewCard[0]);
                    break;
                case 'deck':
                    scene.socket.emit('action', 'oppMove', 'dropDeck', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.dropDeck(scene.viewCard[0]);
                    break;
                case 'hand':
                    scene.socket.emit('action', 'oppMove', 'dropHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.dropHand(scene.viewCard[0]);
                    break;
                case 'view':
                    scene.socket.emit('action', 'oppMove', 'dropView', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.dropView(scene.viewCard[0]);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.dropToGraveyard.setVisible(false);

        scene.dropToLost = this.renderText('放逐', startX + xGap*2, startY + yGap*5);
        scene.dropToLost.setFontSize(fontSize);
        scene.dropToLost.on('pointerdown', function () {
            switch (scene.viewCard[0].currentZone) {
                case 'trainer':
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                case 'stadium':
                    scene.socket.emit('action', 'oppMove', 'vanishField', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.vanishField(scene.viewCard[0]);
                    break;
                case 'deck':
                    scene.socket.emit('action', 'oppMove', 'vanishDeck', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.vanishDeck(scene.viewCard[0]);
                    break;
                case 'hand':
                    scene.socket.emit('action', 'oppMove', 'vanishHand', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.vanishHand(scene.viewCard[0]);
                    break;
                case 'view':
                    scene.socket.emit('action', 'oppMove', 'vanishView', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.vanishView(scene.viewCard[0]);
                    break;
                case 'graveyard':
                    scene.socket.emit('action', 'oppMove', 'vanishGraveyard', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
                    scene.effect.vanishGraveyard(scene.viewCard[0]);
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }
            scene.viewer.allTextOff(true);
        });
        scene.dropToLost.setVisible(false);

        scene.showCard = this.renderText('展示', startX + xGap*2, startY + yGap*2);
        scene.showCard.setFontSize(fontSize);
        scene.showCard.on('pointerdown', function () {
            scene.socket.emit('action', 'oppMove', 'showCard', scene.operation.buildEmitData(scene.viewCard[0]), scene.player);
            // scene.viewer.allTextOff(true);
        });
        scene.showCard.setVisible(false);

        scene.flipPrize = this.renderText('反轉', startX + xGap*1, startY + yGap*2);
        scene.flipPrize.setFontSize(fontSize);
        scene.flipPrize.on('pointerdown', function () {
            //loop prize and find the one has same texture key as viewcard
            for (let i = 0; i < 8; i++) {
                if (i == scene.viewCard[0].index) {
                    scene.viewer.cardList[i].setTexture(scene.viewer.cardList[i].cardData.Image).setDisplaySize(150, 210).setInteractive();
                }
            }
            // scene.viewer.allTextOff(true);
        });
        scene.flipPrize.setVisible(false);

        scene.viewView = this.renderText('檢視', 1180, 400);
        scene.viewView.on('pointerdown', function () {
            scene.viewer.viewAll('view');
        });

        scene.dropView = this.renderText('捨棄', 1180, 500);
        scene.dropView.on('pointerdown', function () {
            let length = scene.gameData.view.length;
            for (let i=0;i<length;i++) {
                if (scene.gameData.view[0].currentZone == 'view') {
                    scene.socket.emit('action', 'oppMove', 'dropView', scene.operation.buildEmitData(scene.gameData.view[0]), scene.player);
                    scene.effect.dropView(scene.gameData.view[0]);
                } 
                else if (scene.gameData.view[0].currentZone == 'deck') {
                    scene.socket.emit('action', 'oppMove', 'dropDeck', scene.operation.buildEmitData(scene.gameData.view[0]), scene.player);
                    scene.effect.dropDeck(scene.gameData.view[0]);
                } 
                else {
                    scene.gameData.view[0].destroy();
                    scene.gameData.view.splice(0, 1);
                }
            }
        });
        scene.dropView.setVisible(false);

        scene.draw1 = this.renderText('抽一', 1380, 470);
        scene.draw1.on('pointerdown', function () {
            scene.effect.drawX(1);
            scene.socket.emit('action', 'oppMove', 'drawX', 1, scene.player);
        });
        // scene.draw1.setVisible(false);

        scene.viewDeck = this.renderText('檢視', 1380, 530);
        scene.viewDeck.on('pointerdown', function () {
            scene.viewer.viewAll('deck');
        });
        scene.viewDeck.setVisible(false);

        scene.topDeck = this.renderText('翻一', 1380, 590);
        scene.topDeck.on('pointerdown', function () {
            let gameObjectArr = scene.effect.searchTopX(1);
            scene.socket.emit('action', 'oppMove', 'searchTopX', gameObjectArr, scene.player);
        });
        scene.topDeck.setVisible(false);

        scene.shuffle = this.renderText('洗牌', 1380, 650);
        scene.shuffle.on('pointerdown', function () {
            scene.gameData[scene.player].deck = scene.operation.shuffleDeck(scene.gameData[scene.player].deck);
        });
        scene.shuffle.setVisible(false);

        scene.getPrize = this.renderText('拿一', 180, 770);
        scene.getPrize.on('pointerdown', function () {
            scene.effect.getPrize();
            scene.socket.emit('action', 'oppMove', 'getPrize', 1, scene.player);
        });

        scene.viewPrize = this.renderText('檢視', 180, 820);
        scene.viewPrize.on('pointerdown', function () {
            scene.viewer.viewAll('prize');
        });
        scene.viewPrize.setVisible(false);

        scene.viewGraveyard = this.renderText('檢視', 1380, 770);
        scene.viewGraveyard.on('pointerdown', function () {
            scene.viewer.viewAll('graveyard');
        });

        scene.viewOppGraveyard = this.renderText('檢視', 180, 110);
        scene.viewOppGraveyard.on('pointerdown', function () {
            scene.viewer.viewAll('oppGraveyard');
        });

        scene.viewLostZone = this.renderText('檢視', 180, 560);
        scene.viewLostZone.on('pointerdown', function () {
            scene.viewer.viewAll('lostZone');
        });

        scene.viewOppLostZone = this.renderText('檢視', 1380, 340);
        scene.viewOppLostZone.on('pointerdown', function () {
            scene.viewer.viewAll('oppLostZone');
        });

        this.playerDeckLeft = scene.add.text(1300 - 20, 570 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.playerDeckLeft.depth = 2;
        this.playerDeckLeftRegtangle = scene.add.rectangle(1300, 570, 60, 40, 0x000000);
        this.playerDeckLeftRegtangle.depth = 1;
        this.playerDeckLeftRegtangle.setVisible(false)

        this.playerGraveyardLeft = scene.add.text(1300 - 20, 770 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.playerGraveyardLeft.depth = 2;
        this.playerGraveyardLeftRegtangle = scene.add.rectangle(1300, 770, 60, 40, 0x000000);
        this.playerGraveyardLeftRegtangle.depth = 1;
        this.playerGraveyardLeftRegtangle.setVisible(false)

        this.playerPrizeLeft = scene.add.text(100 - 20, 770 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.playerPrizeLeft.depth = 2;
        this.playerPrizeLeftRegtangle = scene.add.rectangle(100, 770, 60, 40, 0x000000);
        this.playerPrizeLeftRegtangle.depth = 1;
        this.playerPrizeLeftRegtangle.setVisible(false)

        this.playerLostZoneLeft = scene.add.text(100 - 20, 570 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.playerLostZoneLeft.depth = 2;
        this.playerLostZoneLeftRegtangle = scene.add.rectangle(100, 570, 60, 40, 0x000000);
        this.playerLostZoneLeftRegtangle.depth = 1;
        this.playerLostZoneLeftRegtangle.setVisible(false)

        this.oppDeckLeft = scene.add.text(100 - 20, 350 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.oppDeckLeft.depth = 2;
        this.oppDeckLeftRegtangle = scene.add.rectangle(100, 350, 60, 40, 0x000000);
        this.oppDeckLeftRegtangle.depth = 1;
        this.oppDeckLeftRegtangle.setVisible(false)

        this.oppGraveyardLeft = scene.add.text(100 - 20, 110 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.oppGraveyardLeft.depth = 2;
        this.oppGraveyardLeftRegtangle = scene.add.rectangle(100, 110, 60, 40, 0x000000);
        this.oppGraveyardLeftRegtangle.depth = 1;
        this.oppGraveyardLeftRegtangle.setVisible(false)

        this.oppPrizeLeft = scene.add.text(1300 - 20, 110 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.oppPrizeLeft.depth = 2;
        this.oppPrizeLeftRegtangle = scene.add.rectangle(1300, 110, 60, 40, 0x000000);
        this.oppPrizeLeftRegtangle.depth = 1;
        this.oppPrizeLeftRegtangle.setVisible(false)

        this.oppLostZoneLeft = scene.add.text(1300 - 20, 350 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.oppLostZoneLeft.depth = 2;
        this.oppLostZoneLeftRegtangle = scene.add.rectangle(1300, 350, 60, 40, 0x000000);
        this.oppLostZoneLeftRegtangle.depth = 1;
        this.oppLostZoneLeftRegtangle.setVisible(false)

        this.viewZone = scene.add.text(1100 - 20, 460 - 18, ['']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        this.viewZone.depth = 2;
        this.viewZoneRegtangle = scene.add.rectangle(1100, 460, 60, 40, 0x000000);
        this.viewZoneRegtangle.depth = 1;

        // scene.coin = this.renderText('擲代幣:', 220, 460 - 18).setFontSize(36);
        // scene.coin.on('pointerdown', function () {
        //     scene.operation.tossCoin();
        // });

        scene.anims.create({
            key: 'coinback',
            frames: [
                { key: 'coinfront', duration: 20},
                { key: 'coinback', duration: 20}
            ],
            repeat: 3
        });
        scene.anims.create({
            key: 'coinfront',
            frames: [
                { key: 'coinfront', duration: 20 },
                { key: 'coinback', duration: 20 },
                { key: 'coinfront', duration: 20 }
            ],
            repeat: 2
        });

        scene.coin = scene.add.sprite(300, 460, 'coinfront').setInteractive();
        scene.coin.on('pointerdown', function () {
            scene.operation.tossCoin();
        });

        // scene.coinResult = scene.add.text(340, 460 - 18, ['正']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#c0c0c0');
        
        scene.oppVstar = scene.add.image(300, 320, 'vstar').setDisplaySize(150, 79).setInteractive();
        scene.oppVstar.visible = false;
        scene.selfVstar = scene.add.image(300, 600, 'vstar').setDisplaySize(150, 79).setInteractive();
        scene.selfVstar.on('pointerdown', function () {
            // scene.selfVstar.visible = false;
            if (scene.selfVstar.isTinted) {
                scene.selfVstar.setTint(0xffffff);
            }
            else {
                scene.selfVstar.setTint(0x333333);
            }  
            scene.socket.emit('action', 'oppMove', 'useVstar', '', scene.player);
        });
        scene.selfVstar.visible = false;

        scene.dropAllHand = this.renderText('捨棄所有手牌', 845, 610);
        scene.dropAllHand.on('pointerdown', function () {
            console.log(scene.player);
            console.log(scene.gameData);
            let length = scene.gameData[scene.player].hand.length;
            for (let i=0;i<length;i++){
                scene.socket.emit('action', 'oppMove', 'dropHand', scene.operation.buildEmitData(scene.gameData[scene.player].hand[0]), scene.player);
                scene.effect.dropHand(scene.gameData[scene.player].hand[0]);
            }
            
        });

        scene.showOppCard = scene.add.text(1560, 720, ['^對手展示卡牌^']).setFontSize(36).setFontFamily('Trebuchet MS').setColor('#ffffff');
        scene.showOppCard.visible = false;
        
    }
    
}
