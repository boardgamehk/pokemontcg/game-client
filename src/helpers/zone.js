export default class Zone {
    constructor(scene) {
        let cardW = 150;
        let cardH = 210;
        let battleY = 570;
        let oppBattleY = 350;
        let benchStart = 100;
        let oppBenchY = 130;
        let benchY = 790;
        let gap = 50;
        this.self = {};
        this.opp = {};
        
        this.renderZone = (x, y, width, height) => {
            let dropZone = scene.add.zone(x, y, width, height).setRectangleDropZone(width, height);
            dropZone.setData({ cards: 0 });
            return dropZone;
        };
        this.renderOutline = (dropZone) => {
            let dropZoneOutline = scene.add.graphics();
            dropZoneOutline.lineStyle(4, 0xffffff);
            dropZoneOutline.strokeRect(dropZone.x - dropZone.input.hitArea.width / 2, dropZone.y - dropZone.input.hitArea.height / 2, dropZone.input.hitArea.width, dropZone.input.hitArea.height)
        }

        this.stadium = this.renderZone(benchStart + 2*(cardW + gap), 460, cardW, cardH);
        this.stadiumOutline = this.renderOutline(this.stadium);
        this.stadium.data.name = "stadium";

        this.trainer = this.renderZone(benchStart + 4*(cardW + gap), 460, cardW, cardH);
        this.trainerOutline = this.renderOutline(this.trainer);
        this.trainer.data.name = "trainer";

        this.view = this.renderZone(benchStart + 5*(cardW + gap), 460, cardW, cardH);
        this.viewOutline = this.renderOutline(this.view);
        this.view.data.name = "view";
        // this.view.disableInteractive();

        //Opponent

        this.opp.battleZone = this.renderZone(benchStart + 3*(cardW + gap), oppBattleY, cardW, cardH);
        this.opp.battleZoneOutline = this.renderOutline(this.opp.battleZone);
        this.opp.battleZone.data.name = "opp.battleZone";
        this.opp.battleZone.disableInteractive();
        this.opp.deck = this.renderZone(benchStart, oppBattleY, cardW, cardH);
        this.opp.deckOutline = this.renderOutline(this.opp.deck);
        this.opp.deck.data.name = "opp.deck";
        this.opp.deck.disableInteractive();
        this.opp.graveyard = this.renderZone(benchStart, oppBenchY, cardW, cardH);
        this.opp.graveyardOutline = this.renderOutline(this.opp.graveyard);
        this.opp.graveyard.data.name = "opp.graveyard";
        this.opp.graveyard.disableInteractive();
        this.opp.lostZone = this.renderZone(benchStart + 6*(cardW + gap), oppBattleY, cardW, cardH);
        this.opp.lostZoneOutline = this.renderOutline(this.opp.lostZone);
        this.opp.lostZone.data.name = "opp.lostZone";
        this.opp.lostZone.disableInteractive();

        this.opp.prize = this.renderZone(benchStart + 6*(cardW + gap), oppBenchY, cardW, cardH);
        this.opp.prizeOutline = this.renderOutline(this.opp.prize);
        this.opp.prize.data.name = "opp.prize";
        this.opp.prize.disableInteractive();
        this.opp.benchZone1 = this.renderZone(benchStart + cardW + gap, oppBenchY, cardW, cardH);
        this.opp.benchZone1Outline = this.renderOutline(this.opp.benchZone1);
        this.opp.benchZone1.data.name = "opp.benchZone1";
        this.opp.benchZone1.disableInteractive();
        this.opp.benchZone2 = this.renderZone(benchStart + 2*(cardW + gap), oppBenchY, cardW, cardH);
        this.opp.benchOutline2 = this.renderOutline(this.opp.benchZone2);
        this.opp.benchZone2.data.name = "opp.benchZone2";
        this.opp.benchZone2.disableInteractive();
        this.opp.benchZone3 = this.renderZone(benchStart + 3*(cardW + gap), oppBenchY, cardW, cardH);
        this.opp.benchOutline3 = this.renderOutline(this.opp.benchZone3);
        this.opp.benchZone3.data.name = "opp.benchZone3";
        this.opp.benchZone3.disableInteractive();
        this.opp.benchZone4 = this.renderZone(benchStart + 4*(cardW + gap), oppBenchY, cardW, cardH);
        this.opp.benchOutline4 = this.renderOutline(this.opp.benchZone4);
        this.opp.benchZone4.data.name = "opp.benchZone4";
        this.opp.benchZone4.disableInteractive();
        this.opp.benchZone5 = this.renderZone(benchStart + 5*(cardW + gap), oppBenchY, cardW, cardH);
        this.opp.benchOutline5 = this.renderOutline(this.opp.benchZone5);
        this.opp.benchZone5.data.name = "opp.benchZone5";
        this.opp.benchZone5.disableInteractive();
        


        //You
        this.self.battleZone = this.renderZone(benchStart + 3*(cardW + gap), battleY, cardW, cardH);
        this.self.battleOutline = this.renderOutline(this.self.battleZone);
        this.self.battleZone.data.name = "self.battleZone";
        this.self.deck = this.renderZone(benchStart + 6*(cardW + gap), battleY, cardW, cardH);
        this.self.deckOutline = this.renderOutline(this.self.deck);
        this.self.deck.data.name = "self.deck";
        this.self.deck.disableInteractive();
        this.self.graveyard = this.renderZone(benchStart + 6*(cardW + gap), benchY, cardW, cardH);
        this.self.graveyardOutline = this.renderOutline(this.self.graveyard);
        this.self.graveyard.data.name = "self.graveyard";
        this.self.lostZone = this.renderZone(benchStart, battleY, cardW, cardH);
        this.self.lostZoneOutline = this.renderOutline(this.self.lostZone);
        this.self.lostZone.data.name = "self.lostZone";
        // this.self.graveyard.disableInteractive();
        this.self.prize = this.renderZone(benchStart, benchY, cardW, cardH);
        this.self.prizeOutline = this.renderOutline(this.self.prize);
        this.self.prize.data.name = "self.prize";
        this.self.prize.disableInteractive();
        this.self.benchZone1 = this.renderZone(benchStart + cardW + gap, benchY, cardW, cardH);
        this.self.benchOutline1 = this.renderOutline(this.self.benchZone1);
        this.self.benchZone1.data.name = "self.benchZone1";
        this.self.benchZone2 = this.renderZone(benchStart + 2*(cardW + gap), benchY, cardW, cardH);
        this.self.benchOutline2 = this.renderOutline(this.self.benchZone2);
        this.self.benchZone2.data.name = "self.benchZone2";
        this.self.benchZone3 = this.renderZone(benchStart + 3*(cardW + gap), benchY, cardW, cardH);
        this.self.benchOutline3 = this.renderOutline(this.self.benchZone3);
        this.self.benchZone3.data.name = "self.benchZone3";
        this.self.benchZone4 = this.renderZone(benchStart + 4*(cardW + gap), benchY, cardW, cardH);
        this.self.benchOutline4 = this.renderOutline(this.self.benchZone4);
        this.self.benchZone4.data.name = "self.benchZone4";
        this.self.benchZone5 = this.renderZone(benchStart + 5*(cardW + gap), benchY, cardW, cardH);
        this.self.benchOutline5 = this.renderOutline(this.self.benchZone5);
        this.self.benchZone5.data.name = "self.benchZone5";

        this.self.battleZone.damage = 0;
        this.self.benchZone1.damage = 0;
        this.self.benchZone2.damage = 0;
        this.self.benchZone3.damage = 0;
        this.self.benchZone4.damage = 0;
        this.self.benchZone5.damage = 0;
        this.opp.battleZone.damage = 0;
        this.opp.benchZone1.damage = 0;
        this.opp.benchZone2.damage = 0;
        this.opp.benchZone3.damage = 0;
        this.opp.benchZone4.damage = 0;
        this.opp.benchZone5.damage = 0;

        this.self.battleZone.status = [];
        this.self.benchZone1.status = [];
        this.self.benchZone2.status = [];
        this.self.benchZone3.status = [];
        this.self.benchZone4.status = [];
        this.self.benchZone5.status = [];
        this.opp.battleZone.status = [];
        this.opp.benchZone1.status = [];
        this.opp.benchZone2.status = [];
        this.opp.benchZone3.status = [];
        this.opp.benchZone4.status = [];
        this.opp.benchZone5.status = [];
    }
}