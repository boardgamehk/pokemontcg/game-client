import Card from './card'

export default class Dealer {
    constructor(scene) {
        let playerCard = new Card(scene);

        this.tossCoin = () => {
            let result;
            (Math.floor(Math.random() * 2) == 0) ? result = true : result = false;
            scene.socket.emit('action', 'tossCoin', result);
        }
        this.playerSprite = () => {
            if (scene.isPlayerA) {
                return 'cyanCardBack';
            } 
            return 'magentaCardBack';
        }
        this.opponentSprite = () => {
            if (scene.isPlayerA) {
                return 'magentaCardBack';
            } 
            return 'cyanCardBack';
        }
        this.createCard = (cardData, zone, x, y) => {
            let pCard;
            pCard = playerCard.render(x, y, cardData.Image).setInteractive();
            pCard.cardData = cardData;
            pCard.isMain = true;
            pCard.currentZone = zone;
            return pCard;
        }
        this.createCardTween = (cardData, zone, fromX, fromY, toX, toY, cardBack) => {
            let pCard;
            if (cardBack) {
                pCard = playerCard.render(fromX, fromY, cardBack).setInteractive();
            }
            else {
                pCard = playerCard.render(fromX, fromY, cardData.Image).setInteractive();
            }
            
            pCard.cardData = cardData;
            pCard.currentZone = zone;
            scene.tweens.add({
                targets: pCard,
                x: toX,
                y: toY,
                duration: 500,
                onComplete: function () { 
                    scene.operation.relocateHand();
                    if (!scene.singleMode) {
                        scene.operation.relocateOppHand();
                    }
                    scene.viewer.setCardView(pCard);
                    this.remove();
                },
            });
            return pCard;
        }
        this.dropTrainer = () => {
            if (scene.gameData.trainer.cardData) {
                let graveyardZone;
                if (scene.gameData.trainer.cardData.owner == scene.player) {
                    graveyardZone = scene.zone.self.graveyard;
                }
                else {
                    graveyardZone = scene.zone.opp.graveyard;
                }
                let pCard = playerCard.render(graveyardZone.x, graveyardZone.y, scene.gameData.trainer.cardData.Image).setInteractive();
                pCard.cardData = scene.gameData.trainer.cardData;
                pCard.currentZone = "graveyard";
                scene.gameData[scene.gameData.trainer.cardData.owner].graveyard.push(pCard);
                scene.gameData.trainer.destroy();
            }
        }
        this.shuffleDeck = (deck) => {
            let currentIndex = deck.length,  randomIndex;
            // While there remain elements to shuffle.
            while (currentIndex != 0) {
                // Pick a remaining element.
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex--;
                // And swap it with the current element.
                [deck[currentIndex], deck[randomIndex]] = [
                deck[randomIndex], deck[currentIndex]];
            }
            return deck;
        }

        this.relocateView = () => {
            for (const [index, value] of scene.gameData.view.entries()) {
                value.currentZone = 'view';
                value.x = scene.zone.view.x - index*20;
                value.y = scene.zone.view.y - index*20;
            }
            return;
        }

        this.relocateHand = () => {
            // let hands = scene.gameData[scene.player].hand.length;
            // let starting = 100;
            // let gap = 1400/hands;
            // for (let i = 0; i < hands - 1; i++) {
            //     scene.gameData[scene.player].hand[i].currentZone = 'hand';
            //     scene.gameData[scene.player].hand[i].x = starting + (i * gap);
            //     scene.gameData[scene.player].hand[i].y = 980;
            //     // scene.gameData[scene.player].hand[i].setInteractive();
            // }
            let hands = scene.gameData[scene.player].hand.length + 1;
            let starting = (1600 - hands*120) / 2 + 60;
            starting = (1600 - hands*120) / 2 + 60;
            for (let i = 0; i < hands - 1; i++) {
                scene.gameData[scene.player].hand[i].currentZone = 'hand';
                scene.gameData[scene.player].hand[i].x = starting + (i * 120);
                scene.gameData[scene.player].hand[i].y = 980;
                // scene.gameData[scene.player].hand[i].setInteractive();
            }
            return;
        }

        this.relocateOppHand = () => {
            // let hands = scene.gameData[scene.player].hand.length;
            // let starting = 100;
            // let gap = 1400/hands;
            // for (let i = 0; i < hands - 1; i++) {
            //     scene.gameData[scene.player].hand[i].currentZone = 'hand';
            //     scene.gameData[scene.player].hand[i].x = starting + (i * gap);
            //     scene.gameData[scene.player].hand[i].y = -60;
            //     scene.gameData[scene.opponent].hand[i].disableInteractive();
            //     // scene.gameData[scene.player].hand[i].setInteractive();
            // }
            let hands = scene.gameData[scene.opponent].hand.length + 1;
            let starting = (1600 - hands*120) / 2 + 60;
            starting = (1600 - hands*120) / 2 + 60;
            for (let i = 0; i < hands - 1; i++) {
                scene.gameData[scene.opponent].hand[i].x = starting + (i * 120);
                scene.gameData[scene.opponent].hand[i].y = -60;
                scene.gameData[scene.opponent].hand[i].disableInteractive();
            }
            return;
        }
        this.dropCardStack = (gameDataZone) => {
            let graveyardZone = scene.zone.self.graveyard;
            for (let type in scene.gameData[scene.player][gameDataZone].cardStack) {
                let length = scene.gameData[scene.player][gameDataZone].cardStack[type].length;
                for (let i=0;i<length;i++) {
                    let pCard = scene.operation.createCardTween(scene.gameData[scene.player][gameDataZone].cardStack[type][0].cardData, "graveyard", 
                        scene.zone.self[scene.gameData[scene.player][gameDataZone].cardStack[type][0].currentZone].x, scene.zone.self[scene.gameData[scene.player][gameDataZone].cardStack[type][0].currentZone].y, 
                        graveyardZone.x, graveyardZone.y);
                    scene.gameData[scene.player].graveyard.push(pCard);
                    scene.operation.destroyCard(scene.gameData[scene.player][gameDataZone].cardStack[type][0]);
                }
            }
            scene.gameData[scene.player][gameDataZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            return;
        }
        this.dropOppCardStack = (gameDataZone) => {
            let graveyardZone = scene.zone.opp.graveyard;
            for (let type in scene.gameData[scene.opponent][gameDataZone].cardStack) {
                let length = scene.gameData[scene.opponent][gameDataZone].cardStack[type].length;
                for (let i=0;i<length;i++) {
                    let pCard = scene.operation.createCardTween(scene.gameData[scene.opponent][gameDataZone].cardStack[type][0].cardData, "graveyard", 
                        scene.gameData[scene.opponent][gameDataZone].cardStack[type][0].x, scene.gameData[scene.opponent][gameDataZone].cardStack[type][0].y, 
                        graveyardZone.x, graveyardZone.y);
                    scene.gameData[scene.opponent].graveyard.push(pCard);
                    scene.operation.destroyOppCard(scene.gameData[scene.opponent][gameDataZone].cardStack[type][0]);
                }
            }
            scene.gameData[scene.opponent][gameDataZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            return;
        }
        this.relocateCardStack = (gameDataZone) => {
            for (let i=0;i<scene.gameData[scene.player][gameDataZone].cardStack.energy.length;i++) {
                scene.gameData[scene.player][gameDataZone].cardStack.energy[i].setDisplaySize(45, 63);
                scene.gameData[scene.player][gameDataZone].cardStack.energy[i].depth = i;
                scene.tweens.add({
                    targets: scene.gameData[scene.player][gameDataZone].cardStack.energy[i],
                    x: scene.zone.self[gameDataZone].x + 100,
                    y: scene.zone.self[gameDataZone].y - 65 + (scene.gameData[scene.player][gameDataZone].cardStack.energy.length - i - 1)*30,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
            }
            for (let i=0;i<scene.gameData[scene.player][gameDataZone].cardStack.tool.length;i++) {
                scene.gameData[scene.player][gameDataZone].cardStack.tool[i].setDisplaySize(45, 63);
                scene.gameData[scene.player][gameDataZone].cardStack.tool[i].depth = 1;
                scene.tweens.add({
                    targets: scene.gameData[scene.player][gameDataZone].cardStack.tool[i],
                    x: scene.zone.self[gameDataZone].x + 52,
                    y: scene.zone.self[gameDataZone].y + 80,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
            }
            for (let i=0;i<scene.gameData[scene.player][gameDataZone].cardStack.pokemon.length;i++) {
                scene.gameData[scene.player][gameDataZone].cardStack.pokemon[i].setDisplaySize(45, 63);
                scene.gameData[scene.player][gameDataZone].cardStack.pokemon[i].depth = 1;
                scene.tweens.add({
                    targets: scene.gameData[scene.player][gameDataZone].cardStack.pokemon[i],
                    x: scene.zone.self[gameDataZone].x - 97 + 45 * (i + 1),
                    y: scene.zone.self[gameDataZone].y + 80,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
            }
            return;
            
        }
        this.relocateOppCardStack = (gameDataZone) => {
            for (let i=0;i<scene.gameData[scene.opponent][gameDataZone].cardStack.energy.length;i++) {
                scene.gameData[scene.opponent][gameDataZone].cardStack.energy[i].setDisplaySize(45, 63);
                scene.gameData[scene.opponent][gameDataZone].cardStack.energy[i].depth = i;
                scene.tweens.add({
                    targets: scene.gameData[scene.opponent][gameDataZone].cardStack.energy[i],
                    x: scene.zone.opp[gameDataZone].x + 100,
                    y: scene.zone.opp[gameDataZone].y - 65 + (scene.gameData[scene.opponent][gameDataZone].cardStack.energy.length - i - 1)*30,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
            }
            for (let i=0;i<scene.gameData[scene.opponent][gameDataZone].cardStack.tool.length;i++) {
                scene.gameData[scene.opponent][gameDataZone].cardStack.tool[i].setDisplaySize(45, 63);
                scene.gameData[scene.opponent][gameDataZone].cardStack.tool[i].depth = 1;
                scene.tweens.add({
                    targets: scene.gameData[scene.opponent][gameDataZone].cardStack.tool[i],
                    x: scene.zone.opp[gameDataZone].x + 52,
                    y: scene.zone.opp[gameDataZone].y + 80,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
            }
            for (let i=0;i<scene.gameData[scene.opponent][gameDataZone].cardStack.pokemon.length;i++) {
                scene.gameData[scene.opponent][gameDataZone].cardStack.pokemon[i].setDisplaySize(45, 63);
                scene.gameData[scene.opponent][gameDataZone].cardStack.pokemon[i].depth = 1;
                scene.tweens.add({
                    targets: scene.gameData[scene.opponent][gameDataZone].cardStack.pokemon[i],
                    x: scene.zone.opp[gameDataZone].x - 97 + 45 * (i + 1),
                    y: scene.zone.opp[gameDataZone].y + 80,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
            }
            return;
        }
        this.removeZoneStr = (zoneName) => {
            let newZoneName;
            newZoneName = zoneName.replace("self.", "");
            newZoneName = newZoneName.replace("opp.", "");
            return newZoneName;
        } 
        this.destroyCard = (gameObject) => {
            switch (gameObject.currentZone) {
                case 'trainer':
                    scene.gameData.trainer.destroy();
                    scene.gameData.trainer = {};
                    break;
                case 'stadium':
                    scene.gameData.stadium.destroy();
                    scene.gameData.stadium = {};
                    break;
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                    let found = false;
                    for (let [index, value] of scene.gameData[scene.player][gameObject.currentZone].cardStack.energy.entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            value.destroy();
                            scene.gameData[scene.player][gameObject.currentZone].cardStack.energy.splice(index, 1);
                            found =  true;
                            break;
                        }
                    }
                    for (let [index, value] of scene.gameData[scene.player][gameObject.currentZone].cardStack.tool.entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            value.destroy();
                            scene.gameData[scene.player][gameObject.currentZone].cardStack.tool.splice(index, 1);
                            found =  true;
                            break;
                        }
                    }
                    for (let [index, value] of scene.gameData[scene.player][gameObject.currentZone].cardStack.pokemon.entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            value.destroy();
                            scene.gameData[scene.player][gameObject.currentZone].cardStack.pokemon.splice(index, 1);
                            found =  true;
                            break;
                        }
                    }
                    if (!found) {
                        scene.gameData[scene.player][gameObject.currentZone].mainCard.destroy();
                        scene.gameData[scene.player][gameObject.currentZone].mainCard = {};
                    }
                    break;
                case 'view':
                    for (let [index, value] of scene.gameData.view.entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            value.destroy();
                            scene.gameData.view.splice(index, 1);
                            break;
                        }
                    }
                    for (let [index, value] of scene.viewer.cardList.entries()) {
                        if (value.cardData == gameObject.cardData) {
                            value.destroy();
                            scene.viewer.cardList.splice(index, 1);
                            break;
                        }
                    }
                    break; 
                case 'deck':
                case 'graveyard':
                case 'lostZone':
                case 'prize':
                    for (let [index, value] of scene.gameData[scene.player][gameObject.currentZone].entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            value.destroy();
                            scene.gameData[scene.player][gameObject.currentZone].splice(index, 1);
                            break;
                        }
                    }
                    for (let [index, value] of scene.viewer.cardList.entries()) {
                        if (value.cardData == gameObject.cardData) {
                            value.destroy();
                            scene.viewer.cardList.splice(index, 1);
                            break;
                        }
                    }
                    break;
                case 'hand': 
                    for (let [index, value] of scene.gameData[scene.player][gameObject.currentZone].entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            scene.gameData[scene.player][gameObject.currentZone][index].destroy();
                            scene.gameData[scene.player][gameObject.currentZone].splice(index, 1);
                            break;
                        }
                    }
                    // this.relocateHand();
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }


        }
        this.destroyOppCard = (gameObject) => {
            switch (gameObject.currentZone) {
                case 'trainer':
                    scene.gameData.trainer.destroy();
                    scene.gameData.trainer = {};
                    break;
                case 'stadium':
                    scene.gameData.stadium.destroy();
                    scene.gameData.stadium = {};
                    break;
                case 'battleZone':
                case 'benchZone1':
                case 'benchZone2':
                case 'benchZone3':
                case 'benchZone4':
                case 'benchZone5':
                    let found = false;
                    for (let [index, value] of scene.gameData[scene.opponent][gameObject.currentZone].cardStack.energy.entries()) {
                        if (value.texture.key == gameObject.cardData.Image) {
                            value.destroy();
                            scene.gameData[scene.opponent][gameObject.currentZone].cardStack.energy.splice(index, 1);
                            found =  true;
                            break;
                        }
                    }
                    for (let [index, value] of scene.gameData[scene.opponent][gameObject.currentZone].cardStack.tool.entries()) {
                        if (value.texture.key == gameObject.cardData.Image) {
                            value.destroy();
                            scene.gameData[scene.opponent][gameObject.currentZone].cardStack.tool.splice(index, 1);
                            found =  true;
                            break;
                        }
                    }
                    for (let [index, value] of scene.gameData[scene.opponent][gameObject.currentZone].cardStack.pokemon.entries()) {
                        if (value.texture.key == gameObject.cardData.Image) {
                            value.destroy();
                            scene.gameData[scene.opponent][gameObject.currentZone].cardStack.pokemon.splice(index, 1);
                            found =  true;
                            break;
                        }
                    }
                    if (!found) {
                        scene.gameData[scene.opponent][gameObject.currentZone].mainCard.destroy();
                        scene.gameData[scene.opponent][gameObject.currentZone].mainCard = {};
                    }
                    break;
                case 'graveyard':
                    for (let [index, value] of scene.gameData[scene.opponent][gameObject.currentZone].entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            scene.gameData[scene.opponent][gameObject.currentZone][index].destroy();
                            scene.gameData[scene.opponent][gameObject.currentZone].splice(index, 1);
                        }
                    }
                    break;
                case 'view':
                    for (let [index, value] of scene.gameData.view.entries()) {
                        if (value.cardData.Name == gameObject.cardData.Name) {
                            value.destroy();
                            scene.gameData.view.splice(index, 1);
                            break;
                        }
                    }
                    for (let [index, value] of scene.viewer.cardList.entries()) {
                        if (value.cardData == gameObject.cardData) {
                            value.destroy();
                            scene.viewer.cardList.splice(index, 1);
                            break;
                        }
                    }
                    break; 
                case 'deck':
                    //not storing opp carddata now
                    scene.gameData[scene.opponent][gameObject.currentZone][0].destroy();
                    scene.gameData[scene.opponent][gameObject.currentZone].splice(0, 1);
                    break;
                case 'prize':
                    //not storing opp carddata now
                    scene.gameData[scene.opponent][gameObject.currentZone][0].destroy();
                    scene.gameData[scene.opponent][gameObject.currentZone].splice(0, 1);
                    break;
                case 'hand': 
                    scene.gameData[scene.opponent][gameObject.currentZone][0].destroy();
                    scene.gameData[scene.opponent][gameObject.currentZone].splice(0, 1);
                    // scene.operation.relocateOppHand();
                    break;
                default:
                    console.log(scene.viewCard[0].currentZone);
            }

        }
        this.buildEmitData = (gameObject, top) => {
            let obj = {};
            obj.currentZone = gameObject.currentZone;
            obj.cardData = gameObject.cardData;
            obj.isMain = gameObject.isMain;
            if (top != undefined) {
                obj.top = top;
            }
            return obj;
        }
        this.transformDamageText = (zone, self) => {
            let prefix;
            self ? prefix = 'self' : prefix = 'opp';
            return prefix + zone.charAt(0).toUpperCase() + zone.slice(1);
        }
    }
}