import Card from './card'


export default class Deck {
    constructor(scene) {
        const StrapiController = require('../lib/StrapiController.js');
        this.strapi = new StrapiController('https://strapi.ptcg.world');

        this.loadDeck = (deckCode) => {
            this.strapi.GetDeck(deckCode, true)
                .then(deck => {
                    scene.me.deckInfo = deck
                    let expandedDeck = this.strapi.getExpandedDeck(deck)
                    scene.me.deck = expandedDeck.rawData
                    scene.me.deckInfo.ready = true
                })
        }

        this.initDeckSingle = () => {
            scene.player = 'a';
            this.initDeck();
        }

        this.initDeck = (player) => {
            let playerSprite;
            let opponentSprite;
            let deck;
            let oppDeck;

            if (scene.singleMode) {
                scene.player = 'a';
                scene.users[0].deck = scene.me.deck;
            }

            if (scene.player == 'a') {
                deck = scene.users[0].deck;
                if (!scene.singleMode) {
                    oppDeck = scene.users[1].deck;
                }
            }
            else {
                deck = scene.users[1].deck;
                oppDeck = scene.users[0].deck;
            }

            if (scene.isPlayerA) {
                playerSprite = 'cyanCardBack';
                opponentSprite = 'magentaCardBack';
            } else {
                playerSprite = 'magentaCardBack';
                opponentSprite = 'cyanCardBack';
            };

            //reverse deck as render from bottom
            let revDeck = scene.operation.shuffleDeck(deck).reverse();
            for (let i = 0; i < revDeck.length; i++) {
                let playerCard = new Card(scene);
                let pCard = playerCard.render(1300, 570, playerSprite).disableInteractive();
                pCard.currentZone = "deck";
                pCard.isMain = true;
                pCard.cardData = deck[i];
                pCard.cardData.owner = scene.player;
                pCard.cardData.damage = 0;
                pCard.cardData.status = [];
                pCard.cardData.currentAction = '';
                scene.gameData[scene.player].deck.push(pCard);
            }
            if (!scene.singleMode) {
                for (let i = 0; i < 60; i++) {
                    let playerCard = new Card(scene);
                    let pCard = playerCard.render(100, 350, opponentSprite).disableInteractive();
                    scene.gameData[scene.opponent].deck.push(pCard);
                }
            }
        }
    }
}