import Card from './card'

export default class OppMove {
    constructor(scene) {
        let card = new Card(scene);
        let oCard;
        let playerCard = new Card(scene);

        this.playTrainer = (emitAction, sprite) => {
            console.log("playTrainer");
            scene.history.push('對手使用訓練家');

            scene.gameData[scene.opponent].hand.shift().destroy();
            if (scene.gameData.trainer.cardData) {
                console.log(scene.gameData.trainer.cardData);
                let graveyardZone;
                if (scene.gameData.trainer.cardData.owner == scene.player) {
                    graveyardZone = scene.zone.self.graveyard;
                }
                else {
                    graveyardZone = scene.zone.opp.graveyard;
                }
                let pCard = playerCard.render(graveyardZone.x, graveyardZone.y, scene.gameData.trainer.cardData.Image).setInteractive();
                pCard.cardData = scene.gameData.trainer.cardData;
                pCard.currentZone = "graveyard";
                scene.gameData[scene.gameData.trainer.cardData.owner].graveyard.push(pCard);
                scene.gameData.trainer.destroy();
            }
            scene.gameData.trainer = scene.operation.createCardTween(emitAction.cardData, emitAction.currentZone, 800, -60, scene.zone.trainer.x, scene.zone.trainer.y);
            // scene.viewer.setViewable();
            // scene.viewer.setOppCardViewable();

            return;
        }
        this.playStadium = (emitAction, sprite) => {
            console.log("playStadium");
            scene.history.push('對手使用場地');

            scene.gameData[scene.opponent].hand.shift().destroy();
            if (scene.gameData.stadium.cardData) {
                console.log(scene.gameData.stadium.cardData);
                let graveyardZone;
                if (scene.gameData.stadium.cardData.owner == scene.player) {
                    graveyardZone = scene.zone.self.graveyard;
                }
                else {
                    graveyardZone = scene.zone.opp.graveyard;
                }
                let pCard = playerCard.render(graveyardZone.x, graveyardZone.y, scene.gameData.stadium.cardData.Image).setInteractive();
                pCard.cardData = scene.gameData.stadium.cardData;
                pCard.currentZone = "graveyard";
                scene.gameData[scene.gameData.stadium.cardData.owner].graveyard.push(pCard);
                scene.gameData.stadium.destroy();
            }
            scene.gameData.stadium = scene.operation.createCardTween(emitAction.cardData, emitAction.currentZone, 800, -60, scene.zone.stadium.x, scene.zone.stadium.y);
            // scene.viewer.applyAction(scene.gameData.stadium);

            return;
        }
        this.showUsedAbility = (gameObject, text) => {
            console.log("showUsedAbility");
            console.log(gameObject.currentZone);
            if (gameObject.currentZone == 'stadium') {
                scene.damage.stadiumUsed.visible ? scene.damage.stadiumUsed.visible = false : scene.damage.stadiumUsed.visible = true;
            }
            else {
                let zone = gameObject.currentZone.charAt(0).toUpperCase() + gameObject.currentZone.slice(1);
                scene.damage[`opp${zone}Used`].visible ? scene.damage[`opp${zone}Used`].visible = false : scene.damage[`opp${zone}Used`].visible = true;
            }
            return;
        }
        this.showAttackText = (gameObject) => {
            console.log("showAttackText");

            if (gameObject && (scene.damage.abilityText.text != gameObject.cardData.currentAction)) {
                scene.damage.abilityRegtangle.x = scene.zone.opp[gameObject.currentZone].x;
                scene.damage.abilityRegtangle.y = scene.zone.opp[gameObject.currentZone].y;
                scene.damage.abilityText.text = gameObject.cardData.currentAction;
                scene.damage.abilityText.x = scene.zone.opp[gameObject.currentZone].x - 48;
                scene.damage.abilityText.y = scene.zone.opp[gameObject.currentZone].y - 24;
                scene.damage.abilityRegtangle.visible = true;
                scene.damage.abilityText.visible = true;
            }
            else {
                scene.damage.abilityText.text = '';
                scene.damage.abilityRegtangle.visible = false;
                scene.damage.abilityText.visible = false;
            }

            return;
        }
        this.damage = (gameObject) => {
            console.log("damage");
            scene.history.push('對手寶可夢造成傷害');

            let curDamage = parseInt(scene.damage[scene.operation.transformDamageText(gameObject.card.currentZone, false)].text);
            scene.damage[scene.operation.transformDamageText(gameObject.card.currentZone, false)].text = curDamage + gameObject.damage;
            return;
        }
        this.damageCounter = (zone, damage) => {
            console.log("damageCounter");
            scene.history.push('對手寶可夢造成傷害');

            console.log(scene.damage[scene.operation.transformDamageText(zone, false)]);
            let curDamage = parseInt(scene.damage[scene.operation.transformDamageText(zone, false)].text);
            scene.damage[scene.operation.transformDamageText(zone, false)].text = damage;

            return;
        }
        this.setStatus = (status) => {
            console.log("setStatus");
            scene.history.push('對手寶可夢設置狀態');

            if (!status) {
                scene.gameStatus.activePkmStatus[scene.opponent] = [];
            }
            let index = scene.gameStatus.activePkmStatus[scene.opponent].indexOf(status);
            if (index !== -1) {
                scene.gameStatus.activePkmStatus[scene.opponent].splice(index, 1);
            }
            else {
                scene.gameStatus.activePkmStatus[scene.opponent].push(status);
            }
            scene.damage.oppBattleZoneStatus.text = scene.gameStatus.activePkmStatus[scene.opponent];

            return;
        }
        this.getPrize = () => {
            console.log("getPrize");
            scene.history.push('對手拿一prize');

            let prizeZone = scene.zone.opp.prize;
            let pCard = scene.operation.createCardTween(scene.gameData[scene.opponent].prize[0].cardData, 'hand', prizeZone.x, prizeZone.y, 800, -60, scene.operation.opponentSprite());
            scene.gameData[scene.opponent].prize[0].destroy();
            scene.gameData[scene.opponent].hand.push(pCard);
            scene.gameData[scene.opponent].prize.shift();

            return;
        }
        this.swap = (emitAction, sprite) => {
            console.log("opp swap");
            scene.history.push('對手換位');

            let rCurZone = scene.operation.removeZoneStr(emitAction.prevZone);
            let rSwapZone = scene.operation.removeZoneStr(emitAction.currentZone);
            let curCardData =  scene.gameData[scene.opponent][rCurZone].mainCard.cardData;
            let swapCardData =  scene.gameData[scene.opponent][rSwapZone].mainCard.cardData;
            scene.gameData[scene.opponent][rCurZone].mainCard.destroy();
            scene.gameData[scene.opponent][rSwapZone].mainCard.destroy();
            let pCard;
            pCard = scene.operation.createCardTween(swapCardData, rCurZone, scene.zone.opp[rSwapZone].x, scene.zone.opp[rSwapZone].y, scene.zone.opp[rCurZone].x, scene.zone.opp[rCurZone].y);
            pCard.isMain = true;
            scene.gameData[scene.opponent][rCurZone].mainCard = pCard;
            // scene.viewer.setCardView(scene.gameData[scene.opponent][rCurZone].mainCard);
            pCard = scene.operation.createCardTween(curCardData, rSwapZone, scene.zone.opp[rCurZone].x, scene.zone.opp[rCurZone].y, scene.zone.opp[rSwapZone].x, scene.zone.opp[rSwapZone].y);
            pCard.isMain = true;
            scene.gameData[scene.opponent][rSwapZone].mainCard = pCard;
            // scene.viewer.setCardView(scene.gameData[scene.opponent][rSwapZone].mainCard);
            let curCardStack = scene.gameData[scene.opponent][rCurZone].cardStack;
            let swapCardStack = scene.gameData[scene.opponent][rSwapZone].cardStack;
            for (let arr in scene.gameData[scene.opponent][rCurZone].cardStack) {
                for (let [index, value] of scene.gameData[scene.opponent][rCurZone].cardStack[arr].entries()) {
                    value.destroy();
                }
            }
            scene.gameData[scene.opponent][rCurZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            for (let arr in scene.gameData[scene.opponent][rSwapZone].cardStack) {
                for (let [index, value] of scene.gameData[scene.opponent][rSwapZone].cardStack[arr].entries()) {
                    value.destroy();
                }
            }
            scene.gameData[scene.opponent][rSwapZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            for (let arr in swapCardStack) {
                for (let [index, value] of swapCardStack[arr].entries()) {
                    console.log(value)
                    value.cardData = {};
                    value.cardData.Image = value.texture.key;
                    let card = scene.operation.createCard(value.cardData, rCurZone, scene.zone.opp[rSwapZone].x, scene.zone.opp[rSwapZone].y);
                    card.isMain = false;
                    scene.viewer.setCardView(card);
                    scene.gameData[scene.opponent][rCurZone].cardStack[arr].push(card);
                }
            }
            console.log(curCardStack);
            for (let arr in curCardStack) {
                for (let [index, value] of curCardStack[arr].entries()) {
                    value.cardData = {};
                    value.cardData.Image = value.texture.key;
                    let card = scene.operation.createCard(value.cardData, rSwapZone, scene.zone.opp[rCurZone].x, scene.zone.opp[rCurZone].y);
                    card.isMain = false;
                    console.log(card);
                    scene.viewer.setCardView(card);
                    scene.gameData[scene.opponent][rSwapZone].cardStack[arr].push(card);
                }
            }
            scene.operation.relocateOppCardStack(rCurZone);
            scene.operation.relocateOppCardStack(rSwapZone);
            emitAction.prevZone = rCurZone;
            emitAction.currentZone = rSwapZone;
            let rCurZoneDamage = scene.damage[scene.operation.transformDamageText(rCurZone, false)].text;
            let rSwapZoneDamage = scene.damage[scene.operation.transformDamageText(rSwapZone, false)].text;
            this.damageCounter(rCurZone, rSwapZoneDamage);
            this.damageCounter(rSwapZone, rCurZoneDamage);
            this.setStatus();
            this.showAttackText();

            return;
        }
        this.benchToBattle = (emitAction, sprite) => {
            console.log("opp swap");
            scene.history.push('對手後備區寶可夢上場');

            let rCurZone = scene.operation.removeZoneStr(emitAction.prevZone);
            let rSwapZone = scene.operation.removeZoneStr(emitAction.currentZone);
            let curCardData =  scene.gameData[scene.opponent][rCurZone].mainCard.cardData;
            scene.gameData[scene.opponent][rCurZone].mainCard.destroy();
            scene.gameData[scene.opponent][rCurZone].mainCard = {};
            let pCard;
            pCard = scene.operation.createCardTween(curCardData, rSwapZone, scene.zone.opp[rCurZone].x, scene.zone.opp[rCurZone].y, scene.zone.opp[rSwapZone].x, scene.zone.opp[rSwapZone].y);
            pCard.isMain = true;
            scene.gameData[scene.opponent][rSwapZone].mainCard = pCard;
            // scene.viewer.setCardView(scene.gameData[scene.opponent][rSwapZone].mainCard);
            let curCardStack = scene.gameData[scene.opponent][rCurZone].cardStack;
            for (let arr in scene.gameData[scene.opponent][rCurZone].cardStack) {
                for (let [index, value] of scene.gameData[scene.opponent][rCurZone].cardStack[arr].entries()) {
                    value.destroy();
                }
            }
            scene.gameData[scene.opponent][rCurZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            for (let arr in curCardStack) {
                for (let [index, value] of curCardStack[arr].entries()) {
                    value.cardData = {};
                    value.cardData.Image = value.texture.key;
                    let card = scene.operation.createCard(value.cardData, rSwapZone, scene.zone.opp[rCurZone].x, scene.zone.opp[rCurZone].y);
                    card.isMain = false;
                    scene.viewer.setCardView(card);
                    scene.gameData[scene.opponent][rSwapZone].cardStack[arr].push(card);
                }
            }
            scene.operation.relocateOppCardStack(rSwapZone);
            emitAction.prevZone = rCurZone;
            emitAction.currentZone = rSwapZone;

            return;
        }
        this.attachTool = (emitAction, sprite) => {
            console.log("attachTool");
            scene.history.push('對手裝備');

            let x = 800;
            let y = -60;
            if (emitAction.prevZone == 'view') {
                scene.operation.destroyOppCard(emitAction.gameObject);
                x = scene.zone.view.x;
                y = scene.zone.view.y;
            }
            if (emitAction.prevZone == 'hand') {
                scene.gameData[scene.opponent].hand.shift().destroy();
            }
            let newZoneName = scene.operation.removeZoneStr(emitAction.currentZone);
            // let pCard = scene.operation.createCard(emitAction.gameObject.cardData, newZoneName, x, y);
            let pCard = scene.operation.createCardTween(emitAction.gameObject.cardData, newZoneName, x, y, scene.zone.opp[newZoneName].x, scene.zone.opp[newZoneName].y);
            pCard.isMain = false;
            scene.gameData[scene.opponent][emitAction.currentZone].cardStack.tool.push(pCard);
            scene.operation.relocateOppCardStack(newZoneName);
            // scene.viewer.setOppCardViewable();

            return;
        } 
        this.evolve = (emitAction, sprite) => {
            console.log("evolve");
            scene.history.push('對手寶可夢進化');

            let x = 800;
            let y = -60;
            if (emitAction.prevZone == 'view') {
                x = scene.zone.view.x;
                y = scene.zone.view.y;
                scene.operation.destroyCard(emitAction.gameObject);
            }
            if (emitAction.prevZone == 'hand') {
                scene.gameData[scene.opponent].hand.shift().destroy();
            }
            let newZoneName = scene.operation.removeZoneStr(emitAction.currentZone);
            let basePkm = scene.gameData[scene.opponent][newZoneName].mainCard;
            console.log(basePkm)
            scene.gameData[scene.opponent][newZoneName].cardStack.pokemon.push(basePkm);
            console.log(emitAction)
            let pCard = scene.operation.createCardTween(emitAction.gameObject.cardData, newZoneName, x, y, scene.zone.opp[emitAction.currentZone].x, scene.zone.opp[emitAction.currentZone].y);
            pCard.isMain = true;
            scene.gameData[scene.opponent][newZoneName].mainCard = pCard;
            scene.operation.relocateOppCardStack(newZoneName);
            // scene.viewer.setOppCardViewable();
            this.setStatus();

            return;
        } 
        this.chargeEnergy = (emitAction, sprite) => {
            console.log("opp chargeEnergy");
            scene.history.push('對手填能量');

            let x = 800;
            let y = -60;
            if (emitAction.prevZone == 'view') {
                x = scene.zone.view.x;
                y = scene.zone.view.y;
                scene.operation.destroyCard(emitAction.gameObject);
            }
            if (emitAction.prevZone == 'hand') {
                scene.gameData[scene.opponent].hand.shift().destroy();
            }
            let newZoneName = scene.operation.removeZoneStr(emitAction.currentZone);
            console.log(emitAction)
            let pCard = scene.operation.createCardTween(emitAction.gameObject.cardData, newZoneName, x, y, scene.zone.opp[emitAction.currentZone].x, scene.zone.opp[emitAction.currentZone].y);
            scene.gameData[scene.opponent][newZoneName].cardStack.energy.push(pCard);
            scene.operation.relocateOppCardStack(emitAction.currentZone);
            // scene.viewer.setOppCardViewable();

            return;
        }  
        this.handToField = (emitAction, sprite) => {
            console.log("opphandToField");
            scene.history.push('對手手牌打出寶可夢');

            let x = 800;
            let y = -60;
            if (emitAction.prevZone == 'view') {
                x = scene.zone.view.x;
                y = scene.zone.view.y;
                scene.operation.destroyCard(emitAction.gameObject);
            }
            if (emitAction.prevZone == 'hand') {
                scene.gameData[scene.opponent].hand.shift().destroy();
            }
            let newZoneName = scene.operation.removeZoneStr(emitAction.currentZone);
            let start = scene.gameStatus.start.a && scene.gameStatus.start.b;
            if (start) {
                oCard = scene.operation.createCardTween(emitAction.cardData, newZoneName, x, y, scene.zone.opp[newZoneName].x, scene.zone.opp[newZoneName].y);
            }
            else {
                oCard = scene.operation.createCardTween(emitAction.cardData, newZoneName, x, y, scene.zone.opp[newZoneName].x, scene.zone.opp[newZoneName].y, scene.operation.opponentSprite());
            }
            if (emitAction.cardFront) {
                oCard.cardFront = emitAction.cardFront;
            }
            scene.gameData[scene.opponent][emitAction.currentZone].mainCard = oCard;
            scene.gameData[scene.opponent][emitAction.currentZone].mainCard.isMain = true;
            // scene.viewer.setOppCardViewable();

            return;
        }
        this.deckToView = (gameObject) => {
            console.log("deckToField");

            scene.operation.destroyOppCard(gameObject);
            let graveyardZone = scene.zone.opp.graveyard;
            let pCard;
            pCard = scene.operation.createCardTween(gameObject.cardData, "view", graveyardZone.x, graveyardZone.y, scene.zone.view.x, scene.zone.view.y);
            scene.gameData.view.push(pCard);

            return;
        }
        this.gravyardToView = (gameObject) => {
            console.log("gravyardToField");

            scene.operation.destroyOppCard(gameObject);
            let deckZone = scene.zone.opp.deck;
            let pCard;
            pCard = scene.operation.createCardTween(gameObject.cardData, "view", deckZone.x, deckZone.y, scene.zone.view.x, scene.zone.view.y);
            scene.gameData.view.push(pCard);

            return;
        }  
        this.dropField = (gameObject, isPlayerA) => {
            console.log("dropField");
            scene.history.push('對手棄場上卡牌');

            scene.operation.destroyOppCard(gameObject);
            let graveyardZone = scene.zone.opp.graveyard;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, graveyardZone.x, graveyardZone.y);
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, graveyardZone.x, graveyardZone.y);
                if (gameObject.isMain) {
                    scene.operation.dropOppCardStack(gameObject.currentZone);
                }
            }
            scene.gameData[scene.opponent].graveyard.push(pCard);

            return;
        }
        this.vanishField = (gameObject, isPlayerA) => {
            console.log("vanishField");
            scene.history.push('自己場上卡牌放逐');

            scene.operation.destroyOppCard(gameObject);
            let lostZone = scene.zone.opp.lostZone;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, lostZone.x, lostZone.y);
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, lostZone.x, lostZone.y);
                if (gameObject.isMain) {
                    scene.operation.dropOppCardStack(gameObject.currentZone);
                }
            }
            scene.gameData[scene.opponent].lostZone.push(pCard);

            return;
        }
        this.fieldToPrize = (gameObject) => {
            console.log("fieldToPrize");
            scene.history.push('自己場上卡牌放獎賞');

            scene.operation.destroyCard(gameObject);
            let prize = scene.zone.opp.prize;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "prize", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, prize.x, prize.y, scene.operation.opponentSprite());
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "prize", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, prize.x, prize.y,scene.operation.opponentSprite());
                if (gameObject.isMain) {
                    scene.operation.dropCardStack(gameObject.currentZone);
                }
            }
            scene.gameData[scene.opponent].prize.push(pCard);

            return;
        }  
        this.dropDeck = (gameObject, isPlayerA) => {
            console.log("dropDeck");
            scene.history.push('對手牌組棄牌');

            scene.operation.destroyOppCard(gameObject);
            let graveyardZone = scene.zone.opp.graveyard;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, graveyardZone.x, graveyardZone.y);
            scene.gameData[scene.opponent].graveyard.push(pCard);

            return
        }
        this.vanishDeck = (gameObject, isPlayerA) => {
            console.log("vanishDeck");
            scene.history.push('自己牌組放逐');

            scene.operation.destroyOppCard(gameObject);
            let lostZone = scene.zone.opp.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, lostZone.x, lostZone.y);
            scene.gameData[scene.opponent].lostZone.push(pCard);

            return
        }
        this.dropHand = (gameObject, isPlayerA) => {
            console.log("dropHand");
            scene.history.push('對手手牌棄牌');

            scene.operation.destroyOppCard(gameObject);
            let graveyardZone = scene.zone.opp.graveyard;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", 800, -60, graveyardZone.x, graveyardZone.y);
            scene.gameData[scene.opponent].graveyard.push(pCard);
            
            return
        }   
        this.vanishHand = (gameObject, isPlayerA) => {
            console.log("vanishHand");
            scene.history.push('對手手牌放逐');

            scene.operation.destroyOppCard(gameObject);
            let lostZone = scene.zone.opp.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", 800, -60, lostZone.x, lostZone.y);
            scene.gameData[scene.opponent].lostZone.push(pCard);
            
            return
        }   
        this.handToPrize = (gameObject) => {
            console.log("handToPrize");
            scene.history.push('對手手牌放獎賞');

            scene.operation.destroyCard(gameObject);
            let prize = scene.zone.opp.prize;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "prize", 800, -60 , prize.x, prize.y, scene.operation.opponentSprite());
            scene.gameData[scene.opponent].prize.push(pCard);

            return;
        }  
        this.dropView = (gameObject) => {
            console.log("dropView");

            scene.operation.destroyOppCard(gameObject);
            let graveyardZone = scene.zone.opp.graveyard;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.view.x, scene.zone.view.y, graveyardZone.x, graveyardZone.y);
            scene.gameData[scene.opponent].graveyard.push(pCard);

            return;
        }
        this.vanishView = (gameObject) => {
            console.log("vanishView");

            scene.operation.destroyOppCard(gameObject);
            let lostZone = scene.zone.opp.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.view.x, scene.zone.view.y, lostZone.x, lostZone.y);
            scene.gameData[scene.opponent].lostZone.push(pCard);

            return;
        }
        this.vanishGraveyard = (gameObject) => {
            console.log("vanishView");

            scene.operation.destroyOppCard(gameObject);
            let lostZone = scene.zone.opp.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, lostZone.x, lostZone.y);
            scene.gameData[scene.opponent].lostZone.push(pCard);

            return;
        }
        this.fieldToHand = (gameObject, isPlayerA) => {
            console.log("returnFieldToHand");
            scene.history.push('對手場上卡牌上手牌');

            scene.operation.destroyOppCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, starting, -60, scene.operation.opponentSprite());
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, starting, -60, scene.operation.opponentSprite());
            }
            scene.gameData[scene.opponent].hand.push(pCard);
            scene.operation.relocateOppCardStack(gameObject.currentZone);

            return;
        }
        this.gravyardToHand = (gameObject, isPlayerA) => {
            console.log("returnGravyardToHand");
            scene.history.push('對手棄牌卡牌上手牌');

            scene.operation.destroyOppCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, starting, -60, scene.operation.opponentSprite());
            scene.gameData[scene.opponent].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.opponent].hand);

            return;
        }
        this.prizeToHand = (gameObject) => {
            console.log("prizeToHand");
            scene.history.push('對手獎賞卡牌上手牌');

            scene.operation.destroyOppCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.opp[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, starting, 980);
            scene.gameData[scene.opponent].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.player].hand);

            return;
        }
        this.viewToHand = (gameObject) => {
            console.log("viewToHand");

            scene.operation.destroyOppCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.view.x, scene.zone.view.y, starting, -60, scene.operation.opponentSprite());
            scene.gameData[scene.opponent].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.opponent].hand);

            return;
        }
        this.deckToHand = (gameObject, isPlayerA) => {
            console.log("deckToHand");
            scene.history.push('對手牌組卡牌上手牌');

            scene.operation.destroyOppCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, starting, -60, scene.operation.opponentSprite());
            scene.gameData[scene.opponent].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.opponent].hand);

            return;
        }
        this.fieldToDeck = (gameObject, isPlayerA) => {
            console.log("returnFieldToDeck");
            scene.history.push('對手場上卡牌回牌組');

            let top = gameObject.top;
            scene.operation.destroyOppCard(gameObject);
            let deckZone = scene.zone.opp.deck;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, deckZone.x, deckZone.y, scene.operation.opponentSprite());
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, deckZone.x, deckZone.y, scene.operation.opponentSprite());
            }
            if (top) {
                scene.gameData[scene.opponent].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.opponent].deck.push(pCard);
            }
            
            return;
        }
        this.gravyardToDeck = (gameObject, isPlayerA) => {
            console.log("returnGravyardToDeck");
            scene.history.push('對手棄牌回牌組');

            let top = gameObject.top;
            scene.operation.destroyOppCard(gameObject);
            let deckZone = scene.zone.opp.deck;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "deck", scene.zone.opp[gameObject.currentZone].x, scene.zone.opp[gameObject.currentZone].y, deckZone.x, deckZone.y, scene.operation.opponentSprite());
            if (top) {
                scene.gameData[scene.opponent].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.opponent].deck.push(pCard);
            }

            return;
        }
        this.viewToDeck = (gameObject, top) => {
            console.log("viewToDeck");

            scene.operation.destroyOppCard(gameObject);
            let deckZone = scene.zone.opp.deck;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "deck", scene.zone.view.x, scene.zone.view.y, deckZone.x, deckZone.y, scene.operation.opponentSprite());
            if (top) {
                scene.gameData[scene.opponent].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.opponent].deck.push(pCard);
            }

            return;
        }
        this.handToDeck = (gameObject, isPlayerA) => {
            console.log("handToDeck");
            scene.history.push('對手手牌回牌組');

            let top = gameObject.top;
            scene.operation.destroyOppCard(gameObject);
            let deckZone = scene.zone.opp.deck;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "deck", 800, -60, deckZone.x, deckZone.y, scene.operation.opponentSprite());
            if (top) {
                scene.gameData[scene.opponent].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.opponent].deck.push(pCard);
            }

            return;
        }
        this.drawX = (num) => {
            console.log("drawX");
            scene.history.push('對手抽牌');

            for (let i=0;i<num;i++) {
                let pCard = scene.operation.createCardTween(scene.operation.opponentSprite(), "deck", scene.zone.opp.deck.x, scene.zone.opp.deck.y, 800, -60, scene.operation.opponentSprite());
                pCard.depth = 6;
                scene.gameData[scene.opponent].hand.push(pCard);
                scene.gameData[scene.opponent].deck[scene.gameData[scene.opponent].deck.length - 1].destroy();
                scene.gameData[scene.opponent].deck.shift();
            }

            return;
        } 
        this.searchTopX = (gameObjectArr) => {
            console.log("searchTopX");
            scene.history.push('對手搜索首x張牌');
            
            let deckZone = scene.zone.opp.deck;
            for (let i=0;i<gameObjectArr.length;i++) {
                let pCard = scene.operation.createCardTween(gameObjectArr[i].cardData, "deck", deckZone.x, deckZone.y, scene.zone.view.x, scene.zone.view.y, scene.operation.opponentSprite());
                pCard.disableInteractive();
                scene.gameData.view.push(pCard);
                scene.gameData[scene.opponent].deck[scene.gameData[scene.opponent].deck.length - 1].destroy();
                scene.gameData[scene.opponent].deck.shift();
            }

            return;
        } 
        this.searchDeck = () => {
            console.log("searchDeck");
            return
        } 
        this.swapHandToPrize = () => {
            console.log("swapHandToPrize");
            return
        } 
        this.moveEnergy = () => {
            console.log("moveEnergy");
            return
        }
        this.useVstar = () => {
            console.log("useVstar");
            if (scene.oppVstar.isTinted) {
                scene.oppVstar.setTint(0xffffff);
            }
            else {
                scene.oppVstar.setTint(0x333333);
            }  
            return;
        }
        this.targetCard = (card) => {
            console.log("targetCard");

            if (scene.gameData.stadium.cardData) {
                if (scene.gameData.stadium.cardData.Image == card.cardData.Image) {
                    if (scene.gameData.stadium.isTinted) {
                        scene.gameData.stadium.setTint(0xffffff);
                    }
                    else {
                        scene.gameData.stadium.setTint(0x00ff00);
                    } 
                }
            }
            let zoneList = ['battleZone', 'benchZone1', 'benchZone2', 'benchZone3', 'benchZone4', 'benchZone5'];
            for (const element of zoneList) {
                if (scene.gameData[scene.player][element].mainCard.cardData) {
                    if ((scene.gameData[scene.player][element].mainCard.cardData.Image == card.cardData.Image) && (element == card.currentZone)) {
                        if (scene.gameData[scene.player][element].mainCard.isTinted) {
                            scene.gameData[scene.player][element].mainCard.setTint(0xffffff);
                        }
                        else {
                            scene.gameData[scene.player][element].mainCard.setTint(0x00ff00);
                        }      
                    }    
                    else {
                        scene.gameData[scene.player][element].mainCard.setTint(0xffffff);
                    }                
                }
            }

            return;
        }
        this.showCard = (card) => {
            console.log("showCard");

            scene.viewCard.forEach(element => {
                element.destroy();
            });
            scene.viewCard = [];
            let view = new Card(scene);
            let viewCard = view.render(1685, 400, card.cardData.Image).setDisplaySize(440, 616);
            viewCard.on('pointerdown', function () {
                window.open(card.cardData.Image);
            });
            // viewCard.setTint(0xffff8a);
            scene.showOppCard.visible = true;
            scene.viewer.allTextOff();
            scene.viewCard.push(viewCard);
            if (scene.viewCard.length > 1) {
                scene.viewCard[0].destroy();
                scene.viewCard.pop();
            }
            scene.viewCard[0].cardData = card.cardData;
            scene.viewCard[0].currentZone = "";

            return;
        }
    }
}