import Card from './card'

export default class Effect {
    constructor(scene) {
        let playerCard = new Card(scene);

        this.playTrainer = (card, selectedZone, emitAction) => {
            console.log("playTrainer");
            scene.history.push('自己使用訓練家');

            let newZoneName = scene.operation.removeZoneStr(selectedZone.data.name);
            scene.operation.dropTrainer();
            scene.operation.destroyCard(card);
            let pCard = scene.operation.createCardTween(card.cardData, newZoneName, 800, 980, selectedZone.x, selectedZone.y);
            scene.gameData.trainer = pCard;
            scene.gameData.trainer.currentZone = newZoneName;
            emitAction.prevZone = card.currentZone;
            emitAction.currentZone = card.currentZone;
            // scene.viewer.setViewable();
            // scene.viewer.setOppCardViewable();
            // scene.viewer.setCardView(scene.gameData.trainer);

            return emitAction;
        }
        this.playStadium = (card, selectedZone, emitAction) => {
            console.log("playStadium");
            scene.history.push('自己使用場地');

            let newZoneName = scene.operation.removeZoneStr(selectedZone.data.name);
            if (scene.gameData.stadium.cardData) {
                console.log(scene.gameData.stadium.cardData);
                let graveyardZone;
                if (scene.gameData.stadium.cardData.owner == scene.player) {
                    graveyardZone = scene.zone.self.graveyard;
                }
                else {
                    graveyardZone = scene.zone.opp.graveyard;
                }
                let pCard = playerCard.render(graveyardZone.x, graveyardZone.y, scene.gameData.stadium.cardData.Image).setInteractive();
                pCard.cardData = scene.gameData.stadium.cardData;
                pCard.currentZone = "graveyard";
                scene.gameData[scene.gameData.stadium.cardData.owner].graveyard.push(pCard);
                scene.gameData.stadium.destroy();
            }
            scene.operation.destroyCard(card);
            let pCard = scene.operation.createCardTween(card.cardData, newZoneName, 800, 980, selectedZone.x, selectedZone.y);
            scene.gameData.stadium = pCard;
            scene.gameData.stadium.currentZone = newZoneName;
            emitAction.prevZone = card.currentZone;
            emitAction.currentZone = card.currentZone;
            // scene.viewer.setCardView(scene.gameData.stadium);

            return emitAction;
        }
        this.showUsedAbility = (gameObject, text) => {
            console.log("showUsedAbility");
            if (gameObject.currentZone == 'stadium') {
                scene.damage.stadiumUsed.visible ? scene.damage.stadiumUsed.visible = false : scene.damage.stadiumUsed.visible = true;
            }
            else {
                let zone = gameObject.currentZone.charAt(0).toUpperCase() + gameObject.currentZone.slice(1);
                scene.damage[`self${zone}Used`].visible ? scene.damage[`self${zone}Used`].visible = false : scene.damage[`self${zone}Used`].visible = true;
            }
            return;
        }
        this.showAttackText = (gameObject, text) => {
            console.log("showAttackText");
            if (text && (scene.damage.abilityText.text != text)) {
                scene.gameData[gameObject.cardData.owner][gameObject.currentZone].mainCard.cardData.currentAction = text;
                scene.damage.abilityRegtangle.x = scene.zone.self[gameObject.currentZone].x;
                scene.damage.abilityRegtangle.y = scene.zone.self[gameObject.currentZone].y;
                scene.damage.abilityText.text = text;
                scene.damage.abilityText.x = scene.zone.self[gameObject.currentZone].x - 48;
                scene.damage.abilityText.y = scene.zone.self[gameObject.currentZone].y - 24;
                scene.damage.abilityRegtangle.visible = true;
                scene.damage.abilityText.visible = true;
            }
            else {
                scene.damage.abilityText.text = '';
                scene.damage.abilityRegtangle.visible = false;
                scene.damage.abilityText.visible = false;
            }
            return;
        }
        this.damage = (gameObject, damage) => {
            console.log("damage");
            scene.history.push('自己寶可夢造成傷害');
            console.log(scene.damage.selfBattleZone);
            console.log(scene.damage[scene.operation.transformDamageText(gameObject.currentZone, true)]);
            let curDamage = parseInt(scene.damage[scene.operation.transformDamageText(gameObject.currentZone, true)].text);
            scene.damage[scene.operation.transformDamageText(gameObject.currentZone, true)].text = curDamage + damage;
            return;
        }
        this.damageCounter = (zone, damage) => {
            console.log("damageCounter");
            scene.history.push('自己寶可夢造成傷害');
            console.log(scene.damage[scene.operation.transformDamageText(zone, true)]);
            let curDamage = parseInt(scene.damage[scene.operation.transformDamageText(zone, true)].text);
            scene.damage[scene.operation.transformDamageText(zone, true)].text = damage;
            return;
        }
        this.setStatus = (gameObject, status) => {
            console.log("setStatus");
            scene.history.push('自己寶可夢設置狀態');

            console.log(scene.gameStatus.activePkmStatus[scene.player]);
            if (!status) {
                scene.gameStatus.activePkmStatus[scene.player] = []
            }
            let index = scene.gameStatus.activePkmStatus[scene.player].indexOf(status);
            if (index !== -1) {
                scene.gameStatus.activePkmStatus[scene.player].splice(index, 1);
            }
            else {
                scene.gameStatus.activePkmStatus[scene.player].push(status);
            }
            scene.damage.selfBattleZoneStatus.text = scene.gameStatus.activePkmStatus[scene.player];

            return;
        }
        this.getPrize = () => {
            console.log("getPrize");
            scene.history.push('自己拿一prize');

            let prizeZone = scene.zone.self.prize;
            let pCard = scene.operation.createCardTween(scene.gameData[scene.player].prize[0].cardData, 'hand', prizeZone.x, prizeZone.y, 800, 980);
            scene.gameData[scene.player].prize[0].destroy();
            scene.gameData[scene.player].hand.push(pCard);
            scene.gameData[scene.player].prize.shift();
            // scene.viewer.setViewable(scene.gameData[scene.player].hand);

            return;
        }
        this.swap = (curZone, swapZone, emitAction) => {
            console.log("swap");
            scene.history.push('自己換位');
            let rCurZone = scene.operation.removeZoneStr(curZone);
            let rSwapZone = scene.operation.removeZoneStr(swapZone);
            let curCardData =  scene.gameData[scene.player][rCurZone].mainCard.cardData;
            let swapCardData =  scene.gameData[scene.player][rSwapZone].mainCard.cardData;
            scene.gameData[scene.player][rCurZone].mainCard.destroy();
            scene.gameData[scene.player][rSwapZone].mainCard.destroy();
            let pCard;
            pCard = scene.operation.createCardTween(swapCardData, rCurZone, scene.zone.self[rSwapZone].x, scene.zone.self[rSwapZone].y, scene.zone.self[rCurZone].x, scene.zone.self[rCurZone].y);
            pCard.isMain = true;
            scene.gameData[scene.player][rCurZone].mainCard = pCard;
            pCard = scene.operation.createCardTween(curCardData, rSwapZone, scene.zone.self[rCurZone].x, scene.zone.self[rCurZone].y, scene.zone.self[rSwapZone].x, scene.zone.self[rSwapZone].y);
            pCard.isMain = true;
            scene.gameData[scene.player][rSwapZone].mainCard = pCard;
            let curCardStack = scene.gameData[scene.player][rCurZone].cardStack;
            let swapCardStack = scene.gameData[scene.player][rSwapZone].cardStack;
            for (let arr in scene.gameData[scene.player][rCurZone].cardStack) {
                for (let [index, value] of scene.gameData[scene.player][rCurZone].cardStack[arr].entries()) {
                    value.destroy();
                }
            }
            scene.gameData[scene.player][rCurZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            for (let arr in scene.gameData[scene.player][rSwapZone].cardStack) {
                for (let [index, value] of scene.gameData[scene.player][rSwapZone].cardStack[arr].entries()) {
                    value.destroy();
                }
            }
            scene.gameData[scene.player][rSwapZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };
            for (let arr in swapCardStack) {
                for (let [index, value] of swapCardStack[arr].entries()) {
                    console.log(value)
                    let card = scene.operation.createCard(value.cardData, rCurZone, scene.zone.self[rSwapZone].x, scene.zone.self[rSwapZone].y);
                    card.isMain = false;
                    scene.viewer.setCardView(card);
                    scene.gameData[scene.player][rCurZone].cardStack[arr].push(card);
                }
            }
            console.log(curCardStack);
            for (let arr in curCardStack) {
                for (let [index, value] of curCardStack[arr].entries()) {
                    let card = scene.operation.createCard(value.cardData, rSwapZone, scene.zone.self[rCurZone].x, scene.zone.self[rCurZone].y);
                    card.isMain = false;
                    console.log(card);
                    scene.viewer.setCardView(card);
                    scene.gameData[scene.player][rSwapZone].cardStack[arr].push(card);
                }
            }
            scene.operation.relocateCardStack(rCurZone);
            scene.operation.relocateCardStack(rSwapZone);
            emitAction.prevZone = rCurZone;
            emitAction.currentZone = rSwapZone;
            let rCurZoneDamage = scene.damage[scene.operation.transformDamageText(rCurZone, true)].text;
            let rSwapZoneDamage = scene.damage[scene.operation.transformDamageText(rSwapZone, true)].text;
            this.damageCounter(rCurZone, rSwapZoneDamage);
            this.damageCounter(rSwapZone, rCurZoneDamage);
            this.setStatus();
            this.showAttackText();

            return emitAction;
        }
        this.benchToBattle = (curZone, swapZone, emitAction) => {
            console.log("swap");
            scene.history.push('自己後備區寶可夢上場');

            console.log(curZone.currentZone);
            let rCurZone = scene.operation.removeZoneStr(curZone.currentZone);
            let rSwapZone = 'battleZone'
            let curCardData =  scene.gameData[scene.player][rCurZone].mainCard.cardData;
            scene.gameData[scene.player][rCurZone].mainCard.destroy();
            scene.gameData[scene.player][rCurZone].mainCard = {};
            let pCard;
            pCard = scene.operation.createCardTween(curCardData, rSwapZone, scene.zone.self[rCurZone].x, scene.zone.self[rCurZone].y, scene.zone.self[rSwapZone].x, scene.zone.self[rSwapZone].y);
            pCard.isMain = true;
            scene.gameData[scene.player][rSwapZone].mainCard = pCard;
            let curCardStack = scene.gameData[scene.player][rCurZone].cardStack;
            for (let arr in scene.gameData[scene.player][rCurZone].cardStack) {
                for (let [index, value] of scene.gameData[scene.player][rCurZone].cardStack[arr].entries()) {
                    value.destroy();
                }
            }
            scene.gameData[scene.player][rCurZone].cardStack = {
                "energy": [],
                "pokemon": [],
                "tool": []
            };

            for (let arr in curCardStack) {
                for (let [index, value] of curCardStack[arr].entries()) {
                    let card = scene.operation.createCard(value.cardData, rSwapZone, scene.zone.self[rCurZone].x, scene.zone.self[rCurZone].y);
                    card.isMain = false;
                    scene.gameData[scene.player][rSwapZone].cardStack[arr].push(card);
                }
            }
            scene.operation.relocateCardStack(rSwapZone);
            emitAction.prevZone = rCurZone;
            emitAction.currentZone = rSwapZone;

            return emitAction;
        }
        this.attachTool = (card, selectedZone, emitAction) => {
            console.log("attachTool");
            scene.history.push('自己裝備');

            let newZoneName = scene.operation.removeZoneStr(selectedZone.data.name);
            scene.operation.destroyCard(card);
            let pCard = scene.operation.createCardTween(card.cardData, newZoneName, 800, 980, selectedZone.x, selectedZone.y);
            scene.gameData[scene.player][newZoneName].cardStack.tool.push(pCard);
            if (card.currentZone == 'view') {
                scene.gameData.view.pop();
            }
            scene.operation.relocateCardStack(newZoneName);
            emitAction.prevZone = card.currentZone;
            card.currentZone = newZoneName;
            emitAction.gameObject = scene.operation.buildEmitData(card);
            emitAction.currentZone = card.currentZone;
            // scene.viewer.setViewable(scene.gameData[scene.player][newZoneName].cardStack.tool);

            return emitAction;
        } 
        this.evolve = (card, selectedZone, emitAction) => {
            console.log("evolve");
            scene.history.push('自己寶可夢進化');

            let x = 800;
            let y = 980;
            console.log(emitAction)
            if (emitAction.prevZone == 'view') {
                x = scene.zone.view.x;
                y = scene.zone.view.y;
            }
            let newZoneName = scene.operation.removeZoneStr(selectedZone.data.name);
            scene.gameData[scene.player][newZoneName].cardStack.pokemon.push(scene.gameData[scene.player][newZoneName].mainCard);
            emitAction.gameObject = scene.operation.buildEmitData(card);
            scene.operation.destroyCard(card);
            let pCard = scene.operation.createCardTween(card.cardData, newZoneName, x, y, selectedZone.x, selectedZone.y);
            pCard.isMain = true;
            emitAction.prevZone = card.currentZone;
            scene.gameData[scene.player][newZoneName].mainCard = pCard;
            scene.operation.relocateCardStack(newZoneName);
            card.currentZone = newZoneName;
            emitAction.currentZone = card.currentZone;
            // scene.viewer.setCardView(scene.gameData[scene.player][newZoneName].mainCard);
            // scene.viewer.setViewable(scene.gameData[scene.player][newZoneName].cardStack.pokemon);
            scene.operation.relocateView();
            this.setStatus();
            return emitAction;
        } 
        this.chargeEnergy = (card, selectedZone, emitAction) => {
            console.log("chargeEnergy");
            scene.history.push('自己填能量');

            let newZoneName = scene.operation.removeZoneStr(selectedZone.data.name);
            let fromX = 800;
            let fromY = 980;
            console.log(card.currentZone)
            emitAction.prevZone = card.currentZone;
            scene.operation.destroyCard(card);
            if (card.currentZone == 'view') {
                fromX = scene.zone.view.x;
                fromY = scene.zone.view.y;
            }
            let pCard = scene.operation.createCardTween(card.cardData, newZoneName, fromX, fromY, selectedZone.x, selectedZone.y);
            pCard.isMain = false;
            scene.gameData[scene.player][newZoneName].cardStack.energy.push(pCard);
            scene.operation.relocateCardStack(newZoneName);
            emitAction.prevZone = card.currentZone;
            emitAction.gameObject = scene.operation.buildEmitData(card);
            card.currentZone = newZoneName;
            emitAction.currentZone = card.currentZone;
            // scene.viewer.setViewable(scene.gameData[scene.player][newZoneName].cardStack.energy);
            scene.operation.relocateView();

            return emitAction;
        }  
        this.handToField = (card, selectedZone, emitAction) => {
            console.log("handToField");
            scene.history.push('自己手牌打出寶可夢');

            let newZoneName = scene.operation.removeZoneStr(selectedZone.data.name);
            let fromX = 800;
            let fromY = 980;
            emitAction.prevZone = card.currentZone;
            emitAction.gameObject = scene.operation.buildEmitData(card);
            scene.operation.destroyCard(card);
            if (card.currentZone == 'view') {
                fromX = scene.zone.view.x;
                fromY = scene.zone.view.y;
            }
            let pCard = scene.operation.createCardTween(card.cardData, newZoneName, fromX, fromY, selectedZone.x, selectedZone.y);
            pCard.isMain = true;
            scene.gameData[scene.player][newZoneName].mainCard = pCard;
            emitAction.currentZone = newZoneName;
            scene.gameData[scene.player][newZoneName].mainCard.currentZone = newZoneName;
            // scene.viewer.setCardView(scene.gameData[scene.player][newZoneName].mainCard);

            return emitAction;
        }
        this.deckToView = (gameObject) => {
            console.log("deckToField");

            scene.operation.destroyCard(gameObject);
            let graveyardZone = scene.zone.self.graveyard;
            let pCard;
            let index = scene.gameData.view.length;
            pCard = scene.operation.createCardTween(gameObject.cardData, "view", graveyardZone.x, graveyardZone.y, scene.zone.view.x - index*20, scene.zone.view.y - index*20);
            scene.gameData.view.push(pCard);

            return;
        }
        this.gravyardToView = (gameObject) => {
            console.log("gravyardToField");

            scene.operation.destroyCard(gameObject);
            let deckZone = scene.zone.self.deck;
            let pCard;
            let index = scene.gameData.view.length;
            pCard = scene.operation.createCardTween(gameObject.cardData, "view", deckZone.x, deckZone.y, scene.zone.view.x - index*20, scene.zone.view.y - index*20);
            scene.gameData.view.push(pCard);

            return;
        }  
        this.dropField = (gameObject) => {
            console.log("dropField");
            scene.history.push('自己棄場上卡牌');
            
            scene.operation.destroyCard(gameObject);
            let graveyardZone = scene.zone.self.graveyard;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, graveyardZone.x, graveyardZone.y);
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, graveyardZone.x, graveyardZone.y);
                if (gameObject.isMain) {
                    scene.operation.dropCardStack(gameObject.currentZone);
                }
            }
            scene.gameData[scene.player].graveyard.push(pCard);

            return;
        }
        this.vanishField = (gameObject) => {
            console.log("vanishField");
            scene.history.push('自己場上卡牌放逐');
            
            scene.operation.destroyCard(gameObject);
            let lostZone = scene.zone.self.lostZone;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, lostZone.x, lostZone.y);
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, lostZone.x, lostZone.y);
                if (gameObject.isMain) {
                    scene.operation.dropCardStack(gameObject.currentZone);
                }
            }
            scene.gameData[scene.player].lostZone.push(pCard);

            return;
        }
        this.fieldToPrize = (gameObject) => {
            console.log("fieldToPrize");
            scene.history.push('自己場上卡牌放獎賞');

            scene.operation.destroyCard(gameObject);
            let prize = scene.zone.self.prize;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "prize", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, prize.x, prize.y, scene.operation.playerSprite());
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "prize", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, prize.x, prize.y, scene.operation.playerSprite());
                if (gameObject.isMain) {
                    scene.operation.dropCardStack(gameObject.currentZone);
                }
            }
            scene.gameData[scene.player].prize.push(pCard);

            return;
        }  
        this.dropDeck = (gameObject) => {
            console.log("dropDeck");
            scene.history.push('自己牌組棄牌');

            scene.operation.destroyCard(gameObject);
            let graveyardZone = scene.zone.self.graveyard;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, graveyardZone.x, graveyardZone.y);
            scene.gameData[scene.player].graveyard.push(pCard);

            return;
        }
        this.vanishDeck = (gameObject) => {
            console.log("vansihDeck");
            scene.history.push('自己牌組放逐');

            scene.operation.destroyCard(gameObject);
            let lostZone = scene.zone.self.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, lostZone.x, lostZone.y);
            scene.gameData[scene.player].lostZone.push(pCard);

            return;
        }
        this.dropHand = (gameObject) => {
            console.log("dropHand");
            scene.history.push('自己手牌棄牌');

            scene.operation.destroyCard(gameObject);
            let graveyardZone = scene.zone.self.graveyard;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", 800, 980 , graveyardZone.x, graveyardZone.y);
            scene.gameData[scene.player].graveyard.push(pCard);

            return;
        }   
        this.vanishHand = (gameObject) => {
            console.log("vanishHand");
            scene.history.push('自己手牌放逐');

            scene.operation.destroyCard(gameObject);
            let lostZone = scene.zone.self.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", 800, 980 , lostZone.x, lostZone.y);
            scene.gameData[scene.player].lostZone.push(pCard);

            return;
        }  
        this.handToPrize = (gameObject) => {
            console.log("handToPrize");
            scene.history.push('自己手牌放獎賞');

            scene.operation.destroyCard(gameObject);
            let prize = scene.zone.self.prize;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "prize", 800, 980 , prize.x, prize.y, scene.operation.playerSprite());
            scene.gameData[scene.player].prize.push(pCard);

            return;
        }  
        this.dropView = (gameObject) => {
            console.log("dropView");

            scene.operation.destroyCard(gameObject);
            let graveyardZone = scene.zone.self.graveyard;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.view.x, scene.zone.view.y, graveyardZone.x, graveyardZone.y);
            scene.gameData[scene.player].graveyard.push(pCard);

            return;
        }
        this.vanishView = (gameObject) => {
            console.log("vanishView");

            scene.operation.destroyCard(gameObject);
            let lostZone = scene.zone.self.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.view.x, scene.zone.view.y, lostZone.x, lostZone.y);
            scene.gameData[scene.player].lostZone.push(pCard);

            return;
        }
        this.vanishGraveyard = (gameObject) => {
            console.log("vanishGraveyard");

            scene.operation.destroyCard(gameObject);
            let lostZone = scene.zone.self.lostZone;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "lostZone", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, lostZone.x, lostZone.y);
            scene.gameData[scene.player].lostZone.push(pCard);

            return;
        }
        this.fieldToHand = (gameObject) => {
            console.log("returnFieldToHand");
            scene.history.push('自己場上卡牌上手牌');

            scene.operation.destroyCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, starting, 980);
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, starting, 980);
            }
            scene.gameData[scene.player].hand.push(pCard);

            return;
        }
        this.gravyardToHand = (gameObject) => {
            console.log("returnGravyardToHand");
            scene.history.push('自己棄牌卡牌上手牌');

            scene.operation.destroyCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, starting, 980);
            scene.gameData[scene.player].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.player].hand);

            return;
        }
        this.prizeToHand = (gameObject) => {
            console.log("prizeToHand");
            scene.history.push('自己獎賞卡牌上手牌');

            scene.operation.destroyCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, starting, 980);
            scene.gameData[scene.player].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.player].hand);

            return;
        }
        this.viewToHand = (gameObject) => {
            console.log("viewToHand");

            scene.operation.destroyCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.view.x, scene.zone.view.y, starting, 980);
            scene.gameData[scene.player].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.player].hand);

            return;
        }
        this.deckToHand = (gameObject) => {
            console.log("deckToHand");
            scene.history.push('自己牌組卡牌上手牌');

            scene.operation.destroyCard(gameObject);
            let starting = (1600 - 120) / 2 + 60;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "hand", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, starting, 980);
            scene.gameData[scene.player].hand.push(pCard);
            // scene.viewer.setViewable(scene.gameData[scene.player].hand);

            return;
        }
        this.fieldToDeck = (gameObject, top) => {
            console.log("returnFieldToDeck");
            scene.history.push('自己場上卡牌回牌組');

            scene.operation.destroyCard(gameObject);
            let deckZone = scene.zone.self.deck;
            let pCard;
            if ((gameObject.currentZone == 'trainer') || (gameObject.currentZone == 'stadium')) {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone[gameObject.currentZone].x, scene.zone[gameObject.currentZone].y, deckZone.x, deckZone.y, scene.operation.playerSprite());
            }
            else {
                pCard = scene.operation.createCardTween(gameObject.cardData, "graveyard", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, deckZone.x, deckZone.y, scene.operation.playerSprite());
            }
            if (top) {
                scene.gameData[scene.player].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.player].deck.push(pCard);
            }

            return;
        }
        this.gravyardToDeck = (gameObject, top) => {
            console.log("returnGravyardToDeck");
            scene.history.push('自己棄牌回牌組');

            scene.operation.destroyCard(gameObject);
            let deckZone = scene.zone.self.deck;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "deck", scene.zone.self[gameObject.currentZone].x, scene.zone.self[gameObject.currentZone].y, deckZone.x, deckZone.y, scene.operation.playerSprite());
            if (top) {
                scene.gameData[scene.player].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.player].deck.push(pCard);
            }

            return;
        }
        this.viewToDeck = (gameObject, top) => {
            console.log("viewToDeck");

            scene.operation.destroyCard(gameObject);
            let deckZone = scene.zone.self.deck;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "deck", scene.zone.view.x, scene.zone.view.y, deckZone.x, deckZone.y, scene.operation.playerSprite());
            if (top) {
                scene.gameData[scene.player].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.player].deck.push(pCard);
            }

            return;
        }
        this.handToDeck = (gameObject, top) => {
            console.log("handToDeck");
            scene.history.push('自己手牌回牌組');

            scene.operation.destroyCard(gameObject);
            let deckZone = scene.zone.self.deck;
            let pCard = scene.operation.createCardTween(gameObject.cardData, "deck", 800, 980, deckZone.x, deckZone.y, scene.operation.playerSprite());
            if (top) {
                scene.gameData[scene.player].deck.unshift(pCard);
            }
            else {
                scene.gameData[scene.player].deck.push(pCard);
            }

            return;
        }
        this.drawX = (num) => {
            console.log("drawX");
            scene.history.push('自己抽牌');

            for (let i=0;i<num;i++) {
                let pCard = scene.operation.createCardTween(scene.gameData[scene.player].deck[0].cardData, "deck", scene.zone.self.deck.x, scene.zone.self.deck.y, 800, 980);
                pCard.depth = 6;
                pCard.currentZone = 'hand';
                scene.gameData[scene.player].hand.push(pCard);
                scene.gameData[scene.player].deck[scene.gameData[scene.player].deck.length - 1].destroy();
                scene.gameData[scene.player].deck.shift();
            }

            return;
        } 
        this.searchTopX = (num) => {
            console.log("searchTopX");
            scene.history.push('自己搜索首x張牌');

            let deckZone = scene.zone.self.deck;
            let gameObject = {};
            let gameObjectArr = [];
            for (let i=0;i<num;i++) {
                let pCard = scene.operation.createCardTween(scene.gameData[scene.player].deck[0].cardData, "deck", deckZone.x, deckZone.y, scene.zone.view.x, scene.zone.view.y);
                pCard.currentZone = 'view';
                pCard.disableInteractive();
                gameObject.cardData = scene.gameData[scene.player].deck[0].cardData;
                gameObject.currentZone = 'view';
                scene.gameData.view.push(pCard);
                scene.gameData[scene.player].deck[scene.gameData[scene.player].deck.length - 1].destroy();
                scene.gameData[scene.player].deck.shift();

                console.log(gameObject);
                gameObjectArr.push(scene.operation.buildEmitData(gameObject));
            }

            return gameObjectArr;
        } 
        this.searchDeck = () => {
            console.log("searchDeck");
            return
        } 
        this.swapHandToPrize = () => {
            console.log("swapHandToPrize");
            return
        } 
        this.moveEnergy = () => {
            console.log("moveEnergy");
            return
        }
        this.targetCard = (card) => {
            console.log("targetCard");

            if (scene.gameData.stadium.cardData == card.cardData) {
                if (scene.gameData.stadium.isTinted) {
                    scene.gameData.stadium.setTint(0xffffff);
                }
                else {
                    scene.gameData.stadium.setTint(0x00ff00);
                } 
            }
            let zoneList = ['battleZone', 'benchZone1', 'benchZone2', 'benchZone3', 'benchZone4', 'benchZone5'];
            for (const element of zoneList) {
                console.log(scene.gameData[scene.opponent][element].mainCard);
                if (scene.gameData[scene.opponent][element].mainCard.cardData) {
                    if (scene.gameData[scene.opponent][element].mainCard.cardData == card.cardData) {
                        if (scene.gameData[scene.opponent][element].mainCard.isTinted) {
                            scene.gameData[scene.opponent][element].mainCard.setTint(0xffffff);
                        }
                        else {
                            scene.gameData[scene.opponent][element].mainCard.setTint(0x00ff00);
                        }   
                    }
                    else {
                        scene.gameData[scene.opponent][element].mainCard.setTint(0xffffff);
                    }
                }
            }

            return;
        }
    }
}