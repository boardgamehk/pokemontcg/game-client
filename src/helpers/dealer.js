import Card from './card'

export default class Dealer {
    constructor(scene) {
        // let playerCard = new Card(scene);
        // let opponentCard = new Card(scene);
        // let playerSprite;
        // let opponentSprite;

        this.initPrize = () => {

            for (let i=0; i<6; i++) {
                scene.gameData[scene.player].prize.push(scene.gameData[scene.player].deck[scene.gameData[scene.player].deck.length - 1]);
                scene.tweens.add({
                    targets: scene.gameData[scene.player].deck[scene.gameData[scene.player].deck.length - 1],
                    x: 100,
                    y: 790,
                    // y: 790 - i*10,
                    duration: 500,
                    onComplete: function () { 
                        this.remove();
                    },
                });
                scene.gameData[scene.player].deck.pop();
            }
            if (!scene.singleMode) {
                for (let i=0; i<6; i++) {
                    scene.gameData[scene.opponent].prize.push(scene.gameData[scene.opponent].deck[scene.gameData[scene.opponent].deck.length - 1]);
                    scene.tweens.add({
                        targets: scene.gameData[scene.opponent].deck[scene.gameData[scene.opponent].deck.length - 1],
                        x: 100 + 6*(150 + 50),
                        y: 130,
                        // y: 130 + i*10,
                        duration: 500,
                        onComplete: function () { 
                            this.remove();
                        },
                    });
                    scene.gameData[scene.opponent].deck.pop();
                }
            }

        }
        // this.drawCard = (player) => {
        //     let deckZone;
        //     let pCard;
        //     let opponent;
        //     if (scene.player == 'a') {
        //         playerSprite = 'cyanCardBack';
        //         opponentSprite = 'magentaCardBack';
        //         opponent = 'b';
        //     } else {
        //         playerSprite = 'magentaCardBack';
        //         opponentSprite = 'cyanCardBack';
        //         opponent = 'a';
        //     };
            
        //     if (player == scene.player) {
        //         deckZone = scene.zone.self.deck;
        //         scene.gameData[player].deck[scene.gameData[player].deck.length - 1].destroy();
        //         pCard = scene.operation.createCardTween(scene.gameData[player].deck[scene.gameData[player].deck.length - 1].cardData, "hand", deckZone.x, deckZone.y, 800, 980);
        //         scene.gameData[player].hand.push(pCard);
        //         scene.gameData[player].deck.pop();
        //     }
        //     else {
        //         deckZone = scene.zone.opp.deck;
        //         scene.gameData[opponent].deck[scene.gameData[opponent].deck.length - 1].destroy();
        //         pCard = scene.operation.createCardTween(scene.gameData[opponent].deck[scene.gameData[opponent].deck.length - 1].cardData, "hand", deckZone.x, deckZone.y, 800, -60, opponentSprite);
        //         scene.gameData[opponent].hand.push(pCard);
        //         scene.gameData[opponent].deck.pop();
        //     }
        // }
    }
}