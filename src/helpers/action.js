import Card from './card'

export default class Action {
    constructor(scene) {
        let playerCard = new Card(scene);
        let playerSprite;
        let opponentSprite;
        if (scene.isPlayerA) {
            playerSprite = 'cyanCardBack';
            opponentSprite = 'magentaCardBack';
        } else {
            playerSprite = 'magentaCardBack';
            opponentSprite = 'cyanCardBack';
        };
        this.syncPlayers = (users, isPlayerA) => {
            console.log('syncPlayers');
            scene.users = users;
            scene.gameStatus.userJoined = true;
            scene.changeToSingle.setVisible(false);
            var refreshIntervalId = setInterval(() => {
                if (scene?.me?.deckInfo?.ready) {
                    clearInterval(refreshIntervalId);
                    console.log('deckInfo?.ready');
                    scene.socket.emit('action', 'exchangeDeckData', scene.me);
                }
            }, 100);
            return;
        }

        this.exchangeDeckData = (opp) => {
            console.log('updatePlayers');
            scene.turnText.setText('載入牌組中');

            scene.isPlayerA ? scene.player = 'a' : scene.player = 'b';
            scene.isPlayerA ? scene.opponent = 'b' : scene.opponent = 'a';
            for (let [index, value] of scene.users.entries()) {
                if (value.name == scene.me.name) {
                    scene.users[index].deck = scene.me.deck;
                    scene.users[index].ab = scene.player;
                }
                if (value.name == opp.name) {
                    scene.users[index].deck = opp.deck;
                    scene.users[index].ab = scene.opponent
                }
            }
            if (scene.users[0].deck) {
                console.log(scene.users[0].deck)
                scene.users[0].deck.forEach(card => {
                    console.log(card)
                    if (card.Image) {
                        scene.load.image(card.Image, card.Image);
                        scene.cardImages.added.push(card.Image);
                    }
                });
            }
            if (scene.users[1].deck) {
                console.log(scene.users[1].deck)
                scene.cache.json = scene.me.deck
                scene.users[1].deck.forEach(card => {
                    if (card.Image) {
                        scene.load.image(card.Image, card.Image);
                        scene.cardImages.added.push(card.Image);
                    }
                });
            }

            return;
        }
        
        this.initGame = (obj, player) => {
            if (!scene.gameStatus.init) {
                scene.turnText.setText(['  覆蓋一張', '基礎寶可夢', '然後按開始']);
                scene.startInit.setVisible(false);
                scene.gameStatus.init = true;
                scene.oppVstar.visible = true;
                scene.selfVstar.visible = true;
                player == 'a' ? scene.gameStatus.endFirst = 'b' : scene.gameStatus.endFirst = 'a';
                scene.drawText.setVisible(true);
                scene.drawText.setInteractive();
                if (scene.singleMode) {
                    scene.deck.initDeckSingle(player);
                }
                else {
                    scene.deck.initDeck(player);
                }
                
                scene.effect.drawX(7);
                if (!scene.singleMode) {
                    scene.oppMove.drawX(7);
                }
            }
        },
        this.startGame = (obj, isPlayerA) => {
            if (obj.a) {
                scene.gameStatus.start.a = true;
            }
            if (obj.b) {
                scene.gameStatus.start.b = true;
            }
            if (obj.a && obj.b) {
                console.log("Start");
                scene.yourTurn = false;
                scene.drawText.setVisible(false);
                let zoneList = ["battleZone", "benchZone1", "benchZone2", "benchZone3", "benchZone4", "benchZone5"];
                zoneList.forEach(element => {
                    if (scene.gameData[scene.opponent][element].mainCard) {
                        if (scene.gameData[scene.opponent][element].mainCard.cardFront) {
                            let card = playerCard.render(scene.gameData[scene.opponent][element].mainCard.x, scene.gameData[scene.opponent][element].mainCard.y, scene.gameData[scene.opponent][element].mainCard.cardFront);
                            card.cardData = scene.gameData[scene.opponent][element].mainCard.cardData;
                            card.currentZone = element;
                            card.isMain = true;
                            scene.viewer.setCardView(card);
                            scene.gameData[scene.opponent][element].mainCard.destroy();
                            scene.gameData[scene.opponent][element].mainCard = card;
                        
                        }
    
                    }
                });
                // scene.viewer.setOppCardViewable();
                scene.dealer.initPrize();
                scene.turnText.setText('對手的回合');
                this.endTurn(scene.gameStatus.endFirst);
            }
            if (scene.singleMode && obj.a) {
                scene.yourTurn = false;
                scene.drawText.setVisible(false);
                scene.dealer.initPrize();
                this.endTurn('b')
                scene.endText.setVisible(false);
                //scene.viewPrize.setVisible(true);
            }
        },
        this.endTurn = (player) => {
            scene.endText.setVisible(false);
            scene.turnText.setText('對手的回合');
            let opponent;
            player == 'a' ? opponent = 'b' : opponent = 'a';
            scene.viewDeck.setVisible(false);
            scene.topDeck.setVisible(true);
            scene.draw1.setVisible(true);
            scene.shuffle.setVisible(true);
            scene.viewPrize.setVisible(true);
            scene.damage.abilityRegtangle.setVisible(false);
            scene.damage.abilityText.setVisible(false);
            scene.damage.selfBattleZoneUsed.visible = false;
            scene.damage.selfBenchZone1Used.visible = false;
            scene.damage.selfBenchZone2Used.visible = false;
            scene.damage.selfBenchZone3Used.visible = false;
            scene.damage.selfBenchZone4Used.visible = false;
            scene.damage.selfBenchZone5Used.visible = false;
            scene.damage.oppBattleZoneUsed.visible = false;
            scene.damage.oppBenchZone1Used.visible = false;
            scene.damage.oppBenchZone2Used.visible = false;
            scene.damage.oppBenchZone3Used.visible = false;
            scene.damage.oppBenchZone4Used.visible = false;
            scene.damage.oppBenchZone5Used.visible = false;
            scene.damage.stadiumUsed.visible = false;
            scene.operation.dropTrainer();
            if (scene.player != player) {
                scene.yourTurn = true;
                console.log("Your Turn");
                scene.chargedEnergy = false;
                // scene.turnText.setText('  你的回合');
                scene.turnText.setText('');
                scene.endText.setVisible(true);
                scene.endText.setInteractive();
                scene.viewDeck.setVisible(true);
                scene.topDeck.setVisible(true);
                scene.draw1.setVisible(true);
                scene.shuffle.setVisible(true);
                scene.dropView.setVisible(true);
                scene.effect.drawX(1);
            }
            else {
                let length = scene.gameData.view.length;
                for (let i=0;i<length;i++) {
                    scene.socket.emit('action', 'oppMove', 'dropView', scene.operation.buildEmitData(scene.gameData.view[0]), scene.player);
                    scene.effect.dropView(scene.gameData.view[0]);
                }
                scene.oppMove.drawX(1);
            }
        },
        this.oppMove = (effect, obj, player) => {
            scene.operation.relocateHand();
            if (!scene.singleMode) {
                scene.operation.relocateOppHand();
            }
            if (scene.player !== player) {
                scene.oppMove[effect](obj, player);
                // scene.viewer.setOppCardViewable();
            }
        },
        this.tossCoin = (result) => {
            // scene.coinResult.text = result;
            // let randomColor = Math.floor(Math.random()*16777215).toString(16);
            // scene.coinResult.setColor('#'+randomColor);
            if (result) {
                scene.coin.play('coinfront');
            }
            else {
                scene.coin.play('coinback');
            }
        }
    }
}